logLevel := Level.Warn

classpathTypes += "maven-plugin"

addSbtPlugin("org.scoverage" % "sbt-scoverage" % "1.6.1")

addSbtPlugin("org.scoverage" % "sbt-coveralls" % "1.2.7")

resolvers += ("stepsoft-snapshots" at "https://nexus.mcsherrylabs.com/repository/snapshots")

addSbtPlugin("com.typesafe.sbt" % "sbt-native-packager" % "1.3.8-SNAPSHOT" changing())

addSbtPlugin("com.lightbend.akka.grpc" % "sbt-akka-grpc" % "1.0.0")

addSbtPlugin("com.lightbend.sbt" % "sbt-javaagent" % "0.1.4")

addSbtPlugin("com.lightbend.sbt" % "sbt-proguard" % "0.3.0")

addSbtPlugin("ch.epfl.scala" % "sbt-scalafix" % "0.9.19")

//addSbtPlugin("no.arktekk.sbt" % "aether-deploy" % "0.23.0")

//libraryDependencies += "io.packagecloud.maven.wagon" % "maven-packagecloud-wagon" % "0.0.4"

//addSbtPlugin("io.get-coursier" % "sbt-coursier" % "1.1.0-M13-1")

// For code formatting
//addSbtPlugin("com.lucidchart" % "sbt-scalafmt" % "1.15")
