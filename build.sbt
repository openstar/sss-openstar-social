import sbt.Keys.libraryDependencies

credentials += Credentials(Path.userHome / ".ivy2" / ".credentials")
lazy val commonSettings = Seq(
  organization := "com.mcsherrylabs",
  version := "0.4.0-SNAPSHOT",
  scalaVersion := "2.13.4",
  javacOptions ++= Seq("-source", "11", "-target", "11"),
  scalacOptions ++= Seq("-unchecked", "-deprecation"),
  //coursierChecksums := Nil,
  //coursierArtifactsChecksums := Nil,
  publishTo := {
    val nexus = "https://nexus.mcsherrylabs.com/"
    if (isSnapshot.value)
      Some("snapshots" at nexus + "repository/snapshots")
    else
      Some("releases"  at nexus + "repository/releases")
  },
  credentials += sys.env.get("NEXUS_USER").map(userName => Credentials(
    "Sonatype Nexus Repository Manager",
    "nexus.mcsherrylabs.com",
    userName,
    sys.env.getOrElse("NEXUS_PASS", ""))
  ).getOrElse(
    Credentials(Path.userHome / ".ivy2" / ".credentials")
  ),
  updateOptions := updateOptions.value.withGigahorse(false),
  updateOptions := updateOptions.value.withLatestSnapshots(false),
  resolvers += ("stepsoft" at "https://nexus.mcsherrylabs.com/repository/releases"),
  resolvers += ("stepsoft-snapshots" at "https://nexus.mcsherrylabs.com/repository/snapshots"),
  libraryDependencies += "com.mcsherrylabs" %% "openstar-node-jar" % Vers.openstarNodeJarVer,
  libraryDependencies += "org.scalatest" %% "scalatest" % "3.1.2" % Test,
  libraryDependencies += "org.scalacheck" %% "scalacheck" % "1.14.2" % Test,
  libraryDependencies += "org.scalatestplus" %% "scalatestplus-scalacheck" % "3.1.0.0-RC2" % Test
)


lazy val openstar = (project in file("."))
  .aggregate(`openstar-social-node`)

lazy val `openstar-social-node` = (project in file("openstar-social-node"))
  .settings(commonSettings)

