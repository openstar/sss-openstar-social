package sss.openstar

import sss.ancillary.Restarter.AttemptRestart
import sss.ancillary._
import sss.openstar.account.NodeIdentity
import sss.openstar.controller.UserDirectory
import sss.openstar.html5push.Html5PushActor
import sss.openstar.ledger.LedgerCheckpoints.Checkpoint
import sss.openstar.message.{MessageDownloadPersistCache, OutgoingMessageProcessorUtil}
import sss.openstar.nodebuilder.Currencies.Ada
import sss.openstar.nodebuilder._
import sss.openstar.peers.PeerManager.{ChainQuery, IdQuery}
import sss.openstar.rpc.RpcBridgeImpl
import sss.openstar.tools.ClaimIdentity
import sss.openstar.wallet.UnlockedWallet

import java.io.File
import scala.concurrent.Await
import scala.concurrent.duration.DurationInt
import scala.util.{Failure, Success, Try}

/**
 * Created by alan on 6/10/16.
 */
object Main extends Logging {

  trait ClientNode extends PartialNode
    with UIRpcServerBuilder
    with Html5PushServiceBuilder
    with ApiHostHelperBuilder[Ada] {

    val keyFolder = config.getString("memento.folder")
    new File(keyFolder).mkdirs()


    lazy implicit val users = new UserDirectory(keyFolder)
    lazy implicit val confImp = conf
    implicit val currentBlockHeightImp = () => currentBlockHeight()

    implicit val persistCache: MessageDownloadPersistCache


  }

  def main(withArgs: Array[String]): Unit = {

    Try {
      object clientNode extends ClientNode {

        val restarter = new Restarter(withArgs.toList)
        override val args = new ArgumentParser(withArgs)

        implicit override val attemptRestart: AttemptRestart = () => restarter.attemptRestart()

        override lazy val useUpnpFlag: Boolean = !args.contains(noPnpArgName)

        override val phrase: Option[String] = args(passwordParam)

        override implicit val persistCache: MessageDownloadPersistCache = new MessageDownloadPersistCache


        if (restarter.recoveryInProgress) {
          chain.mostRecentCheckpoint() match {
            case None => log.error("No checkpoint to backup to, no state altered")
            case Some(Checkpoint(height, _)) =>
              log.info(s"Importing state from checkpoint $height...")
              chain.deleteRecentBlocks(2)
              chain.importState(height)
          }
        }


        //syncWallets[Starz](users, starzBalanceLedger)
        syncWallets[Ada](users, cardanoLedger)

        override lazy val unlockedApiHostWallet: UnlockedWallet[Ada] = nodesAdaLedgerWallet

        init // <- init delayed until phrase can be initialised.


        lazy override val httpServer = ServerLauncher(
          ServletContext("/console", "", InitServlet(buildConsoleServlet.get, "/*")).toHandler,
          remoteAttachmentsServlet.toHandler,
          //BEWARE $configName IS ACTUALLY the name you want here
          //ServletContext(s"/db", "", InitServlet(buildDbAccessServlet.get, s"/*")).toHandler,
          //ServletContext("/claim", "", InitServlet(buildClaimServlet.get, "/*")).toHandler
        )


        if (chainOriginatorIdsAndKeys.value.forall(_._1 != nodeIdentity.id)) {
          if (accountOpt(nodeIdentity.id, nodeIdentity.tag).isEmpty) {
            ClaimIdentity.claimRemote(claimUrl, nodeIdentity.id, nodeIdentity.publicKey) match {
              case Failure(e) =>
                log.info(s"Claiming ${nodeIdentity.id} at $claimUrl failed.", e)
              case Success(_) =>
                log.info(s"Claiming ${nodeIdentity.id} at $claimUrl succeeded.")
            }
          }
        }

        implicit val outgoingMessageProcessorUtil = new OutgoingMessageProcessorUtil()


        MessageHandlingActorsBuilder.startMessageHandlingActors[Ada](
          buildUnlockedWallet[Ada](nodeIdentity, cardanoLedgerId), nodeIdentity
        )


        println("List cardano wallets")
        users.listUsers.foreach(u => {
          val w = buildWallet[Ada](u, cardanoLedgerId)
          println(s"Who ${w.walletOwner} Bal ${w.balance()}")
        })
        println("Done cardano wallets")

        override lazy val startHttpServiceFlag: Boolean = args.contains(startHttpServiceArgName)

        //Start IF flag set
        Try(startHttpServer()) recover { case e => log.warn("HttpServer not started {}", e) }

        //Bind to the TCP port
        Await.result(net.start(), 10.seconds)
        log.info("Net bound, begin query....")

        quorumMonitor.checkInitialStatus

        synchronization.startSync

        peerManager.addQuery(IdQuery(nodeConfig.peersList ++ seedNodesFromDns map (_.nodeId.id) filterNot (_ == nodeIdentity.id)))

        if (chain.quorumCandidates() contains nodeIdentity.id) {
          peerManager.addQuery(IdQuery(chain.quorumCandidates() - nodeIdentity.id))
        } else {
          peerManager.addQuery(ChainQuery(globalChainId, 15))
        }

        // sends push message to registered subscribers on new message
        Html5PushActor(Html5PushActor.props(html5Push, messageEventBus))


        val buildAdaWallet: NodeIdentity => UnlockedWallet[Ada] = nId => buildUnlockedWallet[Ada](nId, cardanoLedgerId)
        //val buildStarzWallet: NodeIdentity => UnlockedWallet[Starz] = nId => buildUnlockedWallet[Starz](nId, starzBalanceLedgerId)
        override val rpcImpl = new RpcBridgeImpl(
          //buildStarzWallet,
          buildAdaWallet,
          users,
          nodeIdentity,
          config.getString("welcome.message"),
          config.getInt("welcome.amount")
        )

        override lazy val startUiRpcServerFlag: Boolean = args.contains(startRpcServerArgName)

        //start the RpcServer
        startUiRpcServer()


      }

      clientNode

    } recover {
      case e =>
        e.printStackTrace()
        System.exit(-1)
    }
  }
}
