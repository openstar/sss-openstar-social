name := "openstar-social-node"

packageSummary in Linux := "openstar social node"

enablePlugins(JavaAppPackaging) //, DockerPlugin)

//lazy val akkaGrpcVersion = "1.0.0"

//enablePlugins(AkkaGrpcPlugin)

//libraryDependencies += "com.thesamet.scalapb" %% "scalapb-runtime" % scalapb.compiler.Version.scalapbVersion % "protobuf"

mainClass in Compile := Some("sss.openstar.Main")

mappings in Universal += {
  val conf = sourceDirectory.value / "main" / "resources" / "application.conf"
  conf -> "conf/reference.conf"
}

dockerExposedPorts ++= Seq(2000, 2001, 8080, 8443, 7070)

dockerRepository := Some("registry.gitlab.com")

packageName in Docker := "openstar/sss-openstar/node-runner"
