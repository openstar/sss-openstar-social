import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@vaadin/vaadin-ordered-layout/src/vaadin-horizontal-layout.js';
import '@vaadin/vaadin-button/src/vaadin-button.js';
import '@vaadin/vaadin-icons/vaadin-icons.js';
import '@polymer/iron-icon/iron-icon.js';
import '@vaadin/vaadin-ordered-layout/src/vaadin-vertical-layout.js';

class TicTacToe extends PolymerElement {

    static get template() {
        return html`
<style include="shared-styles">
                :host {
                    display: block;
                    height: 100%;
                }

                .ticButton {
                  width: 100px;
                  height: 60px;
                  border-radius: 0px;
                  margin: 0px;
                }
            </style>
<vaadin-vertical-layout theme="none" style="margin: 0px;" id="vaadinVerticalLayout">
 <vaadin-horizontal-layout style="padding: 0px; margin: 0px;">
  <vaadin-button class="ticButton" style="border-bottom: solid 1px; border-right: solid 1px; " id="r1c1"></vaadin-button>
  <vaadin-button class="ticButton" style="border-left: solid 1px; border-bottom: solid 1px; border-right: solid 1px;" id="r1c2"></vaadin-button>
  <vaadin-button class="ticButton" style="border-bottom: solid 1px; border-left: solid 1px;" id="r1c3"></vaadin-button>
 </vaadin-horizontal-layout>
 <vaadin-horizontal-layout>
  <vaadin-button class="ticButton" theme="icon" style="border-bottom: solid 1px; border-top: solid 1px; border-right: solid 1px;" id="r2c1"></vaadin-button>
  <vaadin-button class="ticButton" theme="icon" style="border: solid 1px;" id="r2c2"></vaadin-button>
  <vaadin-button class="ticButton" theme="icon" style="border-bottom: solid 1px; border-left: solid 1px; border-top: solid 1px;" id="r2c3"></vaadin-button>
 </vaadin-horizontal-layout>
 <vaadin-horizontal-layout>
  <vaadin-button class="ticButton" theme="icon" style="border-top: solid 1px; border-right: solid 1px;" id="r3c1"></vaadin-button>
  <vaadin-button class="ticButton" theme="icon" style="border-top: solid 1px; border-right: solid 1px;border-left: solid 1px;" id="r3c2"></vaadin-button>
  <vaadin-button class="ticButton" theme="icon" style="border-left: solid 1px; border-top: solid 1px;" id="r3c3">
   <!-- iron-icon icon="lumo:plus"></iron-icon-->
  </vaadin-button>
 </vaadin-horizontal-layout>
 <vaadin-button theme="spacing" style="margin-left:auto;margin-bottom:auto" id="commitBtn">
  <iron-icon icon="vaadin:paperplane-o" slot="prefix"></iron-icon>Send 
 </vaadin-button>
</vaadin-vertical-layout>
`;
    }

    static get is() {
        return 'tic-tac-toe';
    }

    static get properties() {
        return {
            // Declare your properties here.
        };
    }
}

customElements.define(TicTacToe.is, TicTacToe);
