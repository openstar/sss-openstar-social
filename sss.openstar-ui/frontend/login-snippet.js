import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@vaadin/vaadin-text-field/src/vaadin-text-field.js';
import '@vaadin/vaadin-ordered-layout/src/vaadin-vertical-layout.js';
import '@vaadin/vaadin-button/src/vaadin-button.js';
import '@vaadin/vaadin-icons/vaadin-icons.js';
import '@polymer/iron-icon/iron-icon.js';
import '@vaadin/vaadin-text-field/src/vaadin-password-field.js';
import '@vaadin/vaadin-ordered-layout/src/vaadin-horizontal-layout.js';
import '@vaadin/vaadin-checkbox/src/vaadin-checkbox.js';

class LoginSnippet extends PolymerElement {

    static get template() {
        return html`
<style include="shared-styles">
            :host {
                display: block;
            }
            vaadin-text-field {
              width: 100%;
            }
            vaadin-password-field {
              width: 100%;
            }

          .flatten-lhs {
                border-top-left-radius: 0;
                border-bottom-left-radius: 0;
                margin-right: 8px;
                padding: 0 var(--lumo-space-s) 0 4px;
          }

          .flatten-rhs {
                border-top-right-radius: 0;
                border-bottom-right-radius: 0;
                padding: 0 4px 0 var(--lumo-space-s);
          }
        </style>
<vaadin-vertical-layout style="height: 100%; max-width: 440px; background: white; justify-content: center; padding: var(--lumo-space-xl);" id="loginBkLayout">
 <vaadin-vertical-layout style="width: 100%; align-items: center; justify-content: flex-start;" id="loginLayout">
  <img style="width: 80%; padding-bottom: 2em;" src="images/logo-black.svg">
  <vaadin-text-field placeholder="your identity" width-full id="username">
   <iron-icon icon="vaadin:user-card" slot="prefix"></iron-icon>
  </vaadin-text-field>
  <vaadin-password-field label="" placeholder="password" width-full id="phrase">
   <iron-icon icon="vaadin:key-o" slot="prefix"></iron-icon>
  </vaadin-password-field>
  <vaadin-password-field label="" placeholder="repeat" width-full id="rephrase">
   <iron-icon icon="vaadin:key-o" slot="prefix"></iron-icon>
  </vaadin-password-field>
  <vaadin-checkbox id="shownotify">
       Keep me logged in
   </vaadin-checkbox>
  <vaadin-horizontal-layout>
   <vaadin-button class="flatten-rhs" id="unlockBtn" theme="primary">
    <iron-icon icon="vaadin:unlock" slot="prefix"></iron-icon>Login 
   </vaadin-button>
   <vaadin-button class="flatten-lhs" id="registerBtn">
    <iron-icon icon="vaadin:user-star" slot="prefix"></iron-icon>Register as new 
   </vaadin-button>
  </vaadin-horizontal-layout>
 </vaadin-vertical-layout>
</vaadin-vertical-layout>
`;
    }

    static get is() {
        return 'login-snippet';
    }

    static get properties() {
        return {
            // Declare your properties here.
        };
    }
}

customElements.define(LoginSnippet.is, LoginSnippet);
