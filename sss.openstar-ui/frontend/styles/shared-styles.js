import { styleInclude, globalStylesheet } from "./styler.js";

import "@vaadin/vaadin-lumo-styles/badge";
import "@vaadin/vaadin-lumo-styles/color";
import "@vaadin/vaadin-lumo-styles/style";
import "@vaadin/vaadin-lumo-styles/typography";

import "./components/account-switcher.js";
import "./components/app-bar.js";
import "./components/brand-expression.js";
import "./components/button.js";
import "./components/charts.js";
import "./components/details-drawer.js";
import "./components/floating-action-button";
import "./components/initials.js";
import "./components/list-item.js";
import "./components/navi-drawer.js";
import "./components/navi-icon.js";
import "./components/navi-item.js";
import "./components/navi-menu.js";
import "./components/tab-bar.js";
import "./components/dialog.js";

import "./message.js";
import "./file.js";

globalStylesheet("styles/lumo-border-radius.css");
globalStylesheet("styles/lumo-icon-size.css");
globalStylesheet("styles/lumo-margin.css");
globalStylesheet("styles/lumo-padding.css");
globalStylesheet("styles/lumo-shadow.css");
globalStylesheet("styles/lumo-spacing.css");
globalStylesheet("styles/lumo-typography.css");

globalStylesheet("styles/misc/box-shadow-borders.css");

globalStylesheet("styles/views/dashboard.css");
globalStylesheet("styles/views/grid-view.css");
globalStylesheet("styles/views/report-details.css");
globalStylesheet("styles/views/view-frame.css");

styleInclude(
  "lumo-badge",
  `:root {
    --app-bar-height: var(--lumo-size-xl);

    --navi-drawer-width: calc(var(--lumo-size-m) * 7);
    --navi-drawer-rail-width: calc(var(--lumo-size-m) * 1.75);
    --navi-item-indentation: calc(var(--lumo-icon-size-s) + var(--lumo-space-l));

    --details-drawer-width: calc(var(--lumo-size-m) * 11);

    --transition-duration-s: 160ms;
    --transition-duration-m: 240ms;
    --transition-duration-l: 320ms;

    --lumo-space-r-m: var(--lumo-space-m);
    --lumo-space-r-l: var(--lumo-space-l);
    --lumo-space-r-x: var(--lumo-space-l);
    --lumo-space-wide-r-m: var(--lumo-space-wide-m);
    --lumo-space-wide-r-l: var(--lumo-space-wide-l);
    
    --lumo-border: 1px solid var(--lumo-shade-20pct);

  }

  html {
    --lumo-primary-text-color: rgb(32, 115, 131);
    --lumo-primary-color-50pct: rgba(32, 115, 131, 0.5);
    --lumo-primary-color-10pct: rgba(32, 115, 131, 0.1);
    --lumo-primary-color: hsl(190, 61%, 32%);
    --lumo-box-shadow-xs: 0 1px 4px -1px var(--lumo-shade-30pct);
    --lumo-box-shadow-s: 0 2px 4px -1px var(--lumo-shade-10pct), 0 3px 12px -1px var(--lumo-shade-20pct);
    --lumo-box-shadow-m: 0 2px 6px -1px var(--lumo-shade-10pct), 0 8px 24px -4px var(--lumo-shade-20pct);
    --lumo-box-shadow-l: 0 3px 18px -2px var(--lumo-shade-10pct), 0 12px 48px -6px var(--lumo-shade-20pct);
    --lumo-box-shadow-xl: 0 4px 24px -3px var(--lumo-shade-10pct), 0 18px 64px -8px var(--lumo-shade-20pct);
  }
  
  [theme~="dark"] {
    --lumo-primary-text-color: rgb(23, 159, 186);
    --lumo-primary-color-50pct: rgba(23, 159, 186, 0.5);
    --lumo-primary-color-10pct: rgba(23, 159, 186, 0.1);
    --lumo-primary-color: hsl(190, 78%, 41%);
    --lumo-success-text-color: rgb(11, 193, 87);
    --lumo-success-color-50pct: rgba(11, 193, 87, 0.5);
    --lumo-success-color-10pct: rgba(11, 193, 87, 0.1);
    --lumo-success-color: hsl(145, 89%, 40%);
    --lumo-base-color: hsl(214, 37%, 11%);
  }

  /* Responsive sizing and spacing */
  @media (max-width: 479px) {
    :root {
      --lumo-space-r-x: 0;
    }
  }
  @media (min-width: 480px) and (max-width: 1023px) {
    :root {
      --lumo-space-r-x: var(--lumo-space-m);
    }
  }
  @media (max-width: 1023px) {
    :root {
      --app-bar-height: var(--lumo-size-l);
      --lumo-space-r-m: var(--lumo-space-s);
      --lumo-space-r-l: var(--lumo-space-m);
      --lumo-space-wide-r-m: var(--lumo-space-wide-s);
      --lumo-space-wide-r-l: var(--lumo-space-wide-m);
    }
  }

  html,
  body {
    height: 100%;
    overflow: hidden;
    width: 100%;
  }

  .root {
    background-color: var(--lumo-contrast-5pct);

  }

  .app-header-outer,
  .app-footer-outer {
    z-index: 3;
  }

  .grid-view {
     padding: var(--lumo-space-r-m) var(--lumo-space-r-m) 0 var(--lumo-space-r-m)

  }

  align-right {
      text-align: right;
  }

  vaadin-grid-cell-content {
    padding-left: 10px;
    padding-right: 10px;
    text-overflow: ellipsis;
  }

  .label-ellipsis {
    text-overflow: ellipsis;
    overflow: hidden;
  }

  vaadin-text-field {
    align-self: auto;
  }
  
  #login {
    background: var(--lumo-shade);
    background-image: url('images/intro-bg.svg');
    background-size: cover;
  }
  
  .coin-amount {
    display: flex;
    flex-direction: row;
    align-items: center;
    padding: 0 var(--lumo-space-s);
    background: white;
    border: var(--lumo-border);
    border-radius: 2rem;
    color: var(--lumo-primary-text-color);
  }
  .coin-amount > *:not(:last-child) {
    padding-right: var(--lumo-space-xs);
  }

  .token {
    display: flex;
    align-items: center;
    background: white;
    box-shadow: var(--lumo-box-shadow-s);
    border-radius: 1rem;
    padding: 0 var(--lumo-space-s);
    line-height: 1.6;
  }

  .flatten-lhs {
        border-top-left-radius: 0;
        border-bottom-left-radius: 0;
        margin-right: 8px;
        padding: 0 var(--lumo-space-s) 0 4px;
  }

  .flatten-rhs {
        border-top-right-radius: 0;
        border-bottom-right-radius: 0;
        padding: 0 4px 0 var(--lumo-space-s);
  }

  .send-money {
      display: flex;
      align-items: center;
      border-radius: 5rem;
      margin: 3px;
      padding: 0 0 0 var(--lumo-space-xs);
  }

  vaadin-dialog-overlay > * > div {
    display: flex;
    flex-direction: column;
    height: 100%;
  }
  
  vaadin-dialog-overlay .extendable-image {
    max-height: 100%;
    max-width: 100%;
    object-fit: contain;
  }
  
  .dialog__header {
    background-image: var(--color-gradient-top);
    padding: var(--lumo-space-s) var(--lumo-space-m);
    border-top-left-radius: var(--lumo-border-radius-l);
    border-top-right-radius: var(--lumo-border-radius-l);
  }
  
  .dialog__header h3 {
    margin: var(--lumo-space-xs) 0;
  }
  
  .dialog__content {
    padding: var(--lumo-space-m);
    overflow: auto;
    flex-grow: 1;
  }
  
  .dialog__actions {
    padding: var(--lumo-space-s) var(--lumo-space-m);
    box-shadow: inset 0 1px var(--lumo-shade-20pct);
  }
  .dialog__actions > * {
    margin-right: var(--lumo-space-xs);
  }
  
  .profile-content {
    flex-direction: column;
    padding: var(--lumo-space-s) var(--lumo-space-m);
    box-sizing: border-box;
  }
  
  .profile-view .avatar-actions {
    flex-direction: column;
    flex-grow: 1;
    align-items: center;
    justify-content: center;
  }
  
  .profile-view .avatar-actions > * {
    margin-bottom: var(--lumo-space-s);
  }
  .profile-view .avatar-image img {
    display: none;
  }
  .avatar {
    border-radius: 100%;
    height: var(--lumo-size-m);
    margin: var(--lumo-space-s);
    width: var(--lumo-size-m);
  }
  
  .avatar-image {
    border: var(--lumo-border);
    border-radius: 100px;
    width: 160px;
    height: 160px;
    background-size: cover;
    background-repeat: no-repeat;
    background-position: center;
  }
  .avatar-name {
    font-size: var(--lumo-font-size-l);
    font-weight: 600;
  }

  .centered-layout {
    max-width: 600px;
    margin: 0 auto;
    overflow: auto;
  }
  .centered-layout.align-center {
    align-items: center;
  }
  
  .paywall-category {
    flex-wrap: wrap;
  }
  
  .paywall-category > * {
    margin: 0 var(--lumo-space-xs);
  }
  
  .paywall-category ~ .paywall-category label {
    display: none;
  }
  
  @media (max-width: 800px) {
    .paywall-category {
      background: white;
      border: var(--lumo-border);
      border-radius: var(--lumo-border-radius);
      padding: var(--lumo-space-s);
      margin-top: var(--lumo-space-s);
    }
    .paywall-category vaadin-text-field {
      flex-grow: 1;
    }
  }

`);
