import { style } from "./styler.js";

style(`
  .file {
    display: flex;
    flex-direction: row;
    align-items: center;
    background: white;
    border: var(--lumo-border);
    border-radius: var(--lumo-border-radius);
    width: 100%;
  }
  .file ~ .file {
    margin-top: var(--lumo-space-s);
  }
  .file .file__details {
    display: flex;
    flex-direction: column;
    flex-grow: 1;
    flex-basis: 300px;
    align-self: stretch;
    padding: var(--lumo-space-m);
    border-left: var(--lumo-border);
    overflow-wrap: break-word;
    overflow: auto;
  }
  .file .file__actions {
    display: flex;
    flex-direction: column;
    padding: var(--lumo-space-m);
  }
  .file .file__details .file__title {
    font-weight: 600;
  }
  .file__image-wrapper:hover {
    cursor: pointer;
  }
  .file.file__image img {
    padding: 4px;
    height: auto;
    width: auto;
    max-height: 240px;
    max-width: 100%;
    box-sizing: border-box;
  }
  .file.file__document .file__document-icon {
    padding: 4px;
    width: 80px;
    text-align: center;
  }
  @media (max-width: 800px) {
    .file .file__details {
      font-size: 13px;
    }
  }

`);
