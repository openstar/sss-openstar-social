import { style } from "./styler.js";

style(`
  .msg-write-layout {
    background: white;
    box-shadow: var(--lumo-box-shadow-m);
    padding: var(--lumo-space-s) var(--lumo-space-m);
    z-index: 1;
    overflow: auto;
    min-height: 130px;
  }
  
  .msg-write-layout .file.file__image img {
    max-height: 120px;
  }
  
  .msg-wrapper {
    flex-direction: column;
    overflow: auto;
    background-color: var(--lumo-shade-5pct);
    padding: var(--lumo-space-m) 0;
  }
  
  .msg {
    min-width: 80%;
    flex-shrink: 0;
    max-width: 90%;
    background: white;
    padding: var(--lumo-space-m);
    border-radius: 16px;
    box-shadow: var(--lumo-box-shadow-m);
    margin: var(--lumo-space-s) var(--lumo-space-m);
  }
  
  .msg:not(.msg-own) {
    border-bottom-left-radius: 0;
  }
  
  .msg.msg-own {
    align-self: flex-end;
    border-bottom-right-radius: 0;
    background-image: linear-gradient(330deg, hsl(190, 40%, 99%), hsl(190, 40%, 96%));
  }
  
  .msg-author {
    font-weight: 600;
    font-size: var(--lumo-font-size-s);
  }
  
  .msg-date {
    color: var(--lumo-secondary-text-color);
    font-size: var(--lumo-font-size-s);
    flex-grow: 1;
  }
  
  .msg-body {
    padding: var(--lumo-space-m) 0;
  }
  
  .msg-attachment:not(:empty) {
    border-top: 1px solid var(--lumo-shade-10pct);
    padding-top: var(--lumo-space-m);
  }
  
  .msg-receiver {
    display: flex;
    align-items: center;
    padding: var(--lumo-space-xs) 0;
    color: var(--lumo-secondary-text-color);
  }
  .msg-receiver > *:not(:last-child) {
    padding-right: var(--lumo-space-xs);
  }
  
  @media (max-width: 800px) {
    .msg {
      max-width: 80%;
      margin: var(--lumo-space-s);
    }
    .msg-body {
        padding: var(--lumo-space-xs) 0;
    }
  }

`);
