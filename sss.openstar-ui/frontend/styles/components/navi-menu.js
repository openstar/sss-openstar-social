import { style } from "../styler.js";

style(`
  .navi-menu {
    margin: var(--lumo-space-s) 0;
  }
`);
