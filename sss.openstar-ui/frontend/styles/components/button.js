import { componentStyle } from "../styler.js";

componentStyle(
  { tag: "vaadin-button" },
  `
    :host {
      --lumo-border-radius: calc(var(--lumo-size-m) / 2);
    }
  
    :host([theme~="tertiary-inline"][theme~="icon"]) {
      min-width: 0;
      padding: 0;
    }

    :host(:not([theme~="tertiary"]):not([theme~="tertiary-inline"])){
      box-shadow:inset 0 0 0 1px var(--lumo-contrast-20pct);
    }
    
    :host([theme~="primary"]){
      text-shadow:0 -1px 0 var(--lumo-shade-20pct);
    }
    
    @media (max-width: 800px) {
      :host([theme~="responsive-icon"]) {
        min-width: 0 !important;
        padding: 0 var(--lumo-space-s);
      }
      :host([theme~="responsive-icon"]) [part="prefix"] {
        margin-left: 0;
        margin-right: 0;
      }
      :host([theme~="responsive-icon"]) [part="label"] {
        display: none;
      }
    }
  `
);
