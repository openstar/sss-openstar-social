import { componentStyle } from "../styler.js";

componentStyle(
  { tag: "vaadin-dialog-overlay" },
  `
  [part="content"] {
    padding: 0;
    height: 100%;
  }
  [part="overlay"] {
    overflow: hidden;
  }
  
  @media (max-width: 800px) {
    [part="overlay"] {
      max-width: 90vw;
      max-height: 90vh;
    }
  }
  `
);
