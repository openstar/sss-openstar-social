import { style } from "../styler.js";

style(`
  .initials {
    border-radius: 100%;
    font-weight: 600;
  }
`);
