package sss.openstar.ui.util

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class IdentityUtilSpec extends AnyFlatSpec with Matchers  {

  "String" should "parse in "  in {
    assert(IdentityUtil.clean("Sg asd ") == "sg_asd")
    assert(IdentityUtil.clean("asd ") == "asd")
    assert(IdentityUtil.clean(" Sg asd") == "sg_asd")
    assert(IdentityUtil.clean(" ALL  WRONG") == "all_wrong")
  }


}
