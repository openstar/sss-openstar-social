package sss.openstar.ui.util

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class UploadFilesTrackerSpec extends AnyFlatSpec with Matchers  {

  "UploadFilesTracker" should "behave as follows"  in {
    var sut = UploadFilesTracker(1)
    assert(sut.hasCapacity(1))
    sut = sut.add("a", 1)
    assert(sut.hasName("a"))

    assert(!sut.hasCapacity(1))
    intercept[Exception] {
      sut.add("b", 1)
    }
    assert(sut.hasName("a"))
    assert(!sut.hasName("b"))

    sut = sut.reset
    assert(!sut.hasName("a"))
    assert(!sut.hasName("b"))
    sut = sut.add("a", 1)
    assert(sut.hasName("a"))
    assert(!sut.hasCapacity(1))
    intercept[Exception] {
      sut.add("b", 1)
    }

    sut = sut.remove("a")
    assert(sut.hasCapacity(1))
    sut = sut.add("b", 1)
    assert(sut.hasName("b"))
    assert(!sut.hasName("a"))

  }


}
