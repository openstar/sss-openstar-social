package sss.openstar.ui.util

import org.jsoup.Jsoup
import org.jsoup.safety.Whitelist
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class AutoLinkSpec extends AnyFlatSpec with Matchers {

  val trickyStr = "ajdhkas www.dasda.pl/asdsad?asd=sd www.absda.pl maiandrze@asdsa.pl klajdld http://dsds.pl httpsda http://www.onet.pl https://www.onsdas.plad/dasda"

  "Test JSoup " should "do nothing" in {
    val result = AutoLink.autoLink("www.somewhere.com")
    val cleanText = Jsoup.clean(result, Whitelist.basic().addEnforcedAttribute("a", "target", "_blank"))
    //println(cleanText)
  }

  "www.example.com" should "parse to link" in {
    val result = AutoLink.autoLink("www.somewhere.com")
    assert(result ===
      "<a href=\"http://www.somewhere.com\" target=\"_blank\">www.somewhere.com</a>")
  }

  "http://www.example.com" should "parse to link" in {
    val result = AutoLink.autoLink("http://www.somewhere.com")
    assert(result ===
      "<a href=\"http://www.somewhere.com\" target=\"_blank\">http://www.somewhere.com</a>")

  }

  "https://www.example.com" should "parse to link" in {
    val result = AutoLink.autoLink("https://www.somewhere.com")
    assert(result ===
      "<a href=\"https://www.somewhere.com\" target=\"_blank\">https://www.somewhere.com</a>")

  }

  "alan@openstar.com" should "parse to link" in {
    val result = AutoLink.autoLink("alan@openstar.com")
    assert(result ===
      "<a href=\"mailto:alan@openstar.com\">alan@openstar.com</a>")

  }

  "tricky string" should "parse to links" in {
    val result = AutoLink.autoLink(trickyStr)
    assert(result ===
      """ajdhkas <a href="http://www.dasda.pl/asdsad?asd=sd" target="_blank">www.dasda.pl/asdsad?asd=sd</a> <a href="http://www.absda.pl" target="_blank">www.absda.pl</a> <a href="mailto:maiandrze@asdsa.pl">maiandrze@asdsa.pl</a> klajdld <a href="http://dsds.pl" target="_blank">http://dsds.pl</a> httpsda <a href="http://www.onet.pl" target="_blank">http://www.onet.pl</a> <a href="https://www.onsdas.plad/dasda" target="_blank">https://www.onsdas.plad/dasda</a>"""
    )
  }

}
