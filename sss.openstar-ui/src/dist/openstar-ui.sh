#!/bin/sh

##Execute from the root folder, not the bin folder.

LIB_PATH="./lib/*"
MAIN_CLASS=sss.openstar.Main
CONFIG_FILE="./conf/overrides.conf"
LOGBACK_FILE="./conf/logback.xml"

echo "Using first param as -DKEY_STORE_PASS"

exec java -DKEY_STORE_PASS="$1" -cp "$LIB_PATH" -Dconfig.file=$CONFIG_FILE -Dlogback.configurationFile=$LOGBACK_FILE $MAIN_CLASS "$@"
