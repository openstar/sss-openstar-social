importScripts('/VAADIN/static/server/workbox/workbox-sw.js');

workbox.setConfig({
  modulePathPrefix: '/VAADIN/static/server/workbox/'
});
workbox.precaching.precacheAndRoute([
{ url: 'images/star_only_green_bk_smallest-144x144.png', revision: '311243726' },
{ url: 'images/star_only_green_bk_smallest-192x192.png', revision: '-873774584' },
{ url: 'images/star_only_green_bk_smallest-512x512.png', revision: '-380354141' },
{ url: 'images/star_only_green_bk_smallest-16x16.png', revision: '2046153717' },
{ url: 'offline.html', revision: '581813331' },
{ url: 'manifest.webmanifest', revision: '2075647931' }
]);
self.addEventListener('fetch', function(event) {
  var request = event.request;
  if (request.mode === 'navigate') {
    event.respondWith(
      fetch(request)
        .catch(function() {
          return caches.match('offline.html');
        })
    );
  }
 });

self.addEventListener('notificationclick', function (event) {
   event.notification.close();
   event.waitUntil(clients.matchAll({ type: 'window', includeUncontrolled: true }).then(clientList => {

       for (var i = 0; i < clientList.length; i++) {
         var client = clientList[i];
         if ('focus' in client)
           return client.focus();
       }
       if (clients.openWindow) {
         const urlToOpen = new URL(event.notification.data.url, self.location.origin);
         return clients.openWindow(urlToOpen);
       }
       return;
   }));

});



self.addEventListener('push', function(event) {

  event.waitUntil(clients.matchAll({ type: 'window', includeUncontrolled: true }).then(clientsArr => {

        let notificationTitle = 'Openstar';

        const notificationOptions = {
            icon: 'images/star_only_green_bk_smallest.png',
            vibrate: [200, 100, 200],
            tag: 'openstar-new-msg',
        };

         if (clientsArr !== undefined) {
            let clientIsFocused = false;

           for (let i = 0; i < clientsArr.length; i++) {
             const windowClient = clientsArr[i];
             if (windowClient.focused) {
               clientIsFocused = true;
               break;
             }
           }

           if(!clientIsFocused) {

                if (event.data) {
                  //What is this?
                  const dataText = event.data.text().split(":");
                  notificationOptions.body = dataText[0];
                  notificationOptions.data = {url : dataText[1]} ;
                }
                const notifications = self.registration.getNotifications();
                let currentNotification;


                for(let i = 0; i < notifications.length; i++) {
                  currentNotification = notifications[i];
                  currentNotification.close();
                  notificationTitle = notificationTitle + " " + i;
                }

                return event.waitUntil(
                    self.registration.showNotification(
                      notificationTitle, notificationOptions)
                );
           } else {
              return;
           }
         } else {

         return event.waitUntil(
                 self.registration.showNotification(
                   notificationTitle, notificationOptions)
                );
         }

      }));

});