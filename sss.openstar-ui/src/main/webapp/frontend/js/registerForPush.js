/**
 * Ref https://github.com/web-push-libs/webpush-java/wiki/Usage-Example
 *
 * Step two: The serviceworker is registered (started) in the browser. Now we
 * need to check if push messages and notifications are supported in the browser
 */
function getSubscription() {

    // Check if desktop notifications are supported
    if (!('showNotification' in ServiceWorkerRegistration.prototype)) {
        console.warn('Notifications arent supported.');
        return;
    }

    // Check if user has disabled notifications
    // If a user has manually disabled notifications in his/her browser for
    // your page previously, they will need to MANUALLY go in and turn the
    // permission back on. In this statement you could show some UI element
    // telling the user how to do so.
    if (Notification.permission === 'denied') {
        console.warn('The user has blocked notifications.');
        return;
    }

    // Check is push API is supported
    if (!('PushManager' in window)) {
        console.warn('Push messaging isnt supported.');
        return;
    }

    return navigator.serviceWorker.ready.then(function (serviceWorkerRegistration) {

        // Get the push notification subscription object
        return serviceWorkerRegistration.pushManager.getSubscription().then(function (subscription) {

            // If this is the user's first visit we need to set up
            // a subscription to push notifications
            if (!subscription) {
                return subscribe();
            }

            var key = subscription.getKey ? subscription.getKey('p256dh') : '';
            var auth = subscription.getKey ? subscription.getKey('auth') : '';
            // Update the server state with the new subscription
            return {
               endpoint: subscription.endpoint,
               // Take byte[] and turn it into a base64 encoded string suitable for
               // POSTing to a server over HTTP
               key: key ? btoa(String.fromCharCode.apply(null, new Uint8Array(key))) : '',
               auth: auth ? btoa(String.fromCharCode.apply(null, new Uint8Array(auth))) : ''
           };
        })
        .catch(function(err) {
            // Handle the error - show a notification in the GUI
            console.warn('Error during getSubscription()', err);
        });
    });
}

/**
 * Step three: Create a subscription. Contact the third party push server (for
 * example mozilla's push server) and generate a unique subscription for the
 * current browser.
 */
function subscribe() {

    return navigator.serviceWorker.ready.then(function (serviceWorkerRegistration) {

       const publicKey = new Uint8Array([0x04,0xac,0x80,0xc1,0xc5,0x59,0x8c,0x0c,0x7c,0xbb,0x42,0x40,0xd1,0xf3,0xda,0xe3,0x92,0xe7,0x36,0x8c,0xb0,0x29,0x18,0x2b,0x71,0x5e,0xba,0x10,0x16,0xbf,0xa0,0xb4,0x01,0x15,0x3b,0xb0,0xf3,0x2d,0x7e,0x6b,0x40,0x5c,0x1f,0x8f,0xb3,0x40,0xab,0x85,0xca,0x5e,0x62,0x6d,0x75,0x5a,0x01,0x99,0x45,0x4d,0x81,0x93,0x32,0x32,0x4b,0x4d,0xd8]);

        // Contact the third party push server. Which one is contacted by
        // pushManager is  configured internally in the browser, so we don't
        // need to worry about browser differences here.
        //
        // When .subscribe() is invoked, a notification will be shown in the
        // user's browser, asking the user to accept push notifications from
        // <yoursite.com>. This is why it is async and requires a catch.
        return serviceWorkerRegistration.pushManager.subscribe({
          userVisibleOnly: true,
          applicationServerKey: publicKey
        }).then(function (subscription) {
                      var key = subscription.getKey ? subscription.getKey('p256dh') : '';
                      var auth = subscription.getKey ? subscription.getKey('auth') : '';
                      return {
                                                     endpoint: subscription.endpoint,
                                                     // Take byte[] and turn it into a base64 encoded string suitable for
                                                     // POSTing to a server over HTTP
                                                     key: key ? btoa(String.fromCharCode.apply(null, new Uint8Array(key))) : '',
                                                     auth: auth ? btoa(String.fromCharCode.apply(null, new Uint8Array(auth))) : ''
                                                 };
        })
        .catch(function (e) {
            if (Notification.permission === 'denied') {
                console.warn('Permission for Notifications was denied');
            } else {
                console.error('Unable to subscribe to push.', e);
            }
        });
    });
}
