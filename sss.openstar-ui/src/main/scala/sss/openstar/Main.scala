package sss.openstar

object Main extends Start {

  def main(args: Array[String]): Unit = {
    server
    classlist
    context
    setHandler()
    blockingStart()
  }
}
