package sss.openstar.ui.util

import com.vaadin.flow.component.Component
import com.vaadin.flow.component.html.{Image, Label}
import com.vaadin.flow.component.orderedlayout.{FlexComponent, FlexLayout}
import com.vaadin.flow.data.renderer.ComponentRenderer
import sss.openstar.ui.BaseHelper
import sss.openstar.ui.components.FlexBoxLayout
import sss.openstar.ui.rpc.ResultSeqContacts.RpcContact
import sss.openstar.ui.util.css.FlexDirection

/**
  * Created by juusokantonen on 13/12/2019.
  */
object GridUtil {

  def avatarRenderer[GRID_TYPE](contactProvider: Function[GRID_TYPE, RpcContact]): ComponentRenderer[Component, GRID_TYPE] = {
    new ComponentRenderer[Component, GRID_TYPE](value => {
      val contact: RpcContact = contactProvider.apply(value)
      val avatar = BaseHelper.makeAvatarImage(contact.avatar.map(_.toByteArray))
      makeContactRepresentation(contact.name, avatar)
    })
  }

  def makeContactRepresentation(name: String, avatar: Image): Component = {
    val nameLbl = new Label(name)
    val layout = new FlexBoxLayout(avatar, nameLbl)
    layout.setFlexDirection(FlexLayout.FlexDirection.ROW)
    layout.setAlignItems(FlexComponent.Alignment.CENTER)

    layout
  }
}
