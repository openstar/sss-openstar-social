package sss.openstar.ui.util

import java.awt.Image
import java.io.{ByteArrayOutputStream, InputStream}

import javax.imageio.ImageIO
import org.imgscalr.Scalr

object ImageUtil {


  /**
   * This must be a valid Vaadin image inputStream
   *
   * e.g. Get from upload Buffer via com.vaadin.flow.component.upload.receivers,Upload.getInputStream
   *
   * @param in
   * @param x NB Unsure if this is x!
   * @param y NB Unsure if this is y!
   * @return
   */
  def scaleImageInputStream(in: InputStream, scaleType: Int = Image.SCALE_DEFAULT)(x: Int)(y: Int): Array[Byte] = {

    val yourImage = ImageIO.read(in)
    //val oldSize = createArray(yourImage)
    //println("OLD " + oldSize.size)

    val newImage = Scalr.resize(yourImage, Scalr.Method.SPEED, Scalr.Mode.FIT_TO_WIDTH, x, Scalr.THRESHOLD_BALANCED_SPEED)
    yourImage.flush()

    val os = new ByteArrayOutputStream()
    ImageIO.write(yourImage, "jpg", os);

    //println("NEW W " + newImage.getWidth(null))
    //println("NEW H " + newImage.getHeight(null))
    os.toByteArray
  }

  /*def createArray(image: java.awt.Image):Array[Byte] = {
    val os = new ByteArrayOutputStream()
    ImageIO.write(toBufferedImage(image), "png", os);
    os.toByteArray
  }

  def createInputStream(image: java.awt.Image, x: Int, y: Int): ByteArrayInputStream = {
    val os = new ByteArrayOutputStream()
    ImageIO.write(toBufferedImage(image), "png", os);
    new ByteArrayInputStream(os.toByteArray);
  }

  /*
    https://stackoverflow.com/questions/13605248/java-converting-image-to-bufferedimage
   */
  def toBufferedImage(img: Image): BufferedImage = img match {

    case notBufferedImage =>
      // Create a buffered image with transparency
      val bimage = new BufferedImage(img.getWidth(null), img.getHeight(null), BufferedImage.TYPE_INT_ARGB)
      // Draw the image on to the buffered image
      val bGr = bimage.createGraphics
      bGr.drawImage(img, 0,0, null)
      bGr.dispose()
      // Return the buffered image
      bimage
  }

  /**
   * https://stackoverflow.com/questions/3967731/how-to-improve-the-performance-of-g-drawimage-method-for-resizing-images/11371387#11371387
   *
   * @param originalImage
   * @param biggerWidth
   * @param biggerHeight
   * @return
   */
  def resizeToBig(originalImage: Image, biggerWidth: Int, biggerHeight: Int): Image = {
    val `type` = BufferedImage.TYPE_INT_ARGB


    val resizedImage = new BufferedImage(biggerWidth, biggerHeight, `type`)
    val g = resizedImage.createGraphics();

    g.setComposite(AlphaComposite.Src);
    g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
    g.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
    g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

    g.drawImage(originalImage, 0, 0, biggerWidth, biggerHeight, null);
    g.dispose()


    return resizedImage;
  }*/
}
