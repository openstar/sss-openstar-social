package sss.openstar.ui.util

object IdentityUtil {


  /**
   * Given a userName remove any starting or ending spaces,
   * lowercase it and remove an double spaces.
   * @param userName
   * @return
   */
  def clean(userName: String): String = {
    val noEndOrStartSpaces = userName.trim
    noEndOrStartSpaces.foldLeft("")((acc, e) => {
      (acc, e) match {
        case (acc, ' ') if (acc.nonEmpty && acc.last == '_') => acc
        case (acc, ' ') => acc + '_'
        case _ => acc + e.toLower
      }
    })
  }
}
