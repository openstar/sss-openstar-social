package sss.openstar.ui.util

import java.io.{InputStream, OutputStream}
import java.util.concurrent.atomic.AtomicReference

import com.vaadin.flow.component.upload.MultiFileReceiver
import sss.ancillary.Guid
import sss.openstar.ui.rpc.{BridgeClient, SessionId, UploadFileDetails}

import scala.concurrent.duration._
import scala.concurrent.{Await, Future}
import scala.concurrent.ExecutionContext.Implicits.global

class RpcBridgeMultiFileReceiverFactory(implicit sessId: SessionId,
                                        bridge: BridgeClient) {
  def forGuid(guid: Guid): RpcBridgeMultiFileReceiver = new RpcBridgeMultiFileReceiver(guid, sessId, bridge)
}

class RpcBridgeMultiFileReceiver(val guid: Guid,
                                 sessId: SessionId,
                                 bridge: BridgeClient,
                                 maxWaitDelayForUploadFinish: Duration = 5.seconds) extends MultiFileReceiver {

  @volatile
  private var count: Int = 0

  private val uploadTickets: AtomicReference[Map[String, Future[UploadFileDetails]]] =
    new AtomicReference(Map.empty)


  override def receiveUpload(fileName: String, mimeType: String): OutputStream = {

    synchronized {
      count += 1
    }

    val os = bridge.createAttachmentUploadOutputStream(
      sessId,
      guid.value,
      fileName,
      mimeType)

    uploadTickets.updateAndGet(m => {
      m + (fileName -> os.done)
    })

    os.done.onComplete(_ => {
      uploadTickets.updateAndGet(m => {
        m - fileName
      })
    })

    os.outputStream
  }

  def getInputStream(fileName: String): InputStream = {
    uploadTickets.get().get(fileName).map(fut => Await.result(fut, maxWaitDelayForUploadFinish))
    bridge.downloadAttachment(sessId, guid.value, fileName)
  }

  def remove(fileName: String): Unit = {
    synchronized {
      count -= 1
    }
    bridge.deleteAttachment(sessId, guid.value, fileName)
  }

  def isEmpty:Boolean = synchronized(count == 0)
}

