package sss.openstar.ui.util

import sss.openstar.ui.util.UploadFilesTracker.FileToTrack


object UploadFilesTracker {
  case class FileToTrack(name: String, size: Long)
}

/**
 * Performs no validation on fileName, could be the same or empty
 *
 * @param maxTotalSize
 * @param files
 */
case class UploadFilesTracker(
                               maxTotalSize: Long,
                               private val files: Seq[FileToTrack] = Seq.empty) {


  def add(name: String, size: Long):UploadFilesTracker = {
    add(FileToTrack(name, size))
  }

  def hasCapacity(moreBytes: Long): Boolean = {
    files.map(_.size).sum + moreBytes <= maxTotalSize
  }

  def add(fileToTrack: FileToTrack):UploadFilesTracker = {
    require(hasCapacity(fileToTrack.size), s"$fileToTrack would exceed capacity $maxTotalSize")
    this.copy(files = this.files :+ fileToTrack)
  }

  def remove(name: String): UploadFilesTracker = {
    this.copy(maxTotalSize = this.maxTotalSize, files = this.files.filterNot(_.name == name))
  }

  val hasName: String => Boolean = this.files.map(_.name).contains

  def names: Seq[String] = this.files.map(_.name)

  def reset: UploadFilesTracker = this.copy(files = Seq.empty)

}
