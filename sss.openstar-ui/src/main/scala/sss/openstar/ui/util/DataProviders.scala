package sss.openstar.ui.util

import com.vaadin.flow.data.provider.DataProvider
import sss.openstar.Bridger._
import sss.openstar.ui.rpc.ImplicitOps._
import sss.openstar.ui.rpc.{BridgeClient, DisplayMessage, RegisteredProvider}


class DisplayMessageDataProvider(implicit bridge: BridgeClient) {

  import bridge.maxCallWaitDuration

  private def countMessages(filter: Option[String] = None): Int = {
    val c = bridge.countMessages(SessionTracker.sessId().value)
    c.toInt(0)
  }

  private def pageMessages(page: Int, offset: Int, filter: Option[String] = None): Seq[DisplayMessage] = {
    val r = bridge.fetchMessages(
      SessionTracker.sessId().value,
      page,
      offset,
      true
    )

    r.await.genericHandle()
      .getOrElse(
        Seq.empty
      )
  }

  private lazy val displayMessageImpl = DataProviderUtil.createProviderSupport(pageMessages, countMessages)

  def create: DataProvider[DisplayMessage, Void] = DataProviderUtil[DisplayMessage](displayMessageImpl)

}

class IdentitiesDataProvider(implicit bridge: BridgeClient) {

  private def countMessages(filter: Option[String]): Int =
    bridge
      .countIdentities(filter)
      .genericHandle()
      .map(_.toInt)
      .getOrElse(0)

  private def pageMessages(page: Int, offset: Int, filter: Option[String]): Seq[String] = bridge.fetchIdentities(
    filter,
    page,
    offset
  ).genericHandle()
    .getOrElse(Seq.empty)

  private lazy val impl = DataProviderUtil.createProviderSupport(pageMessages, countMessages)

  def create: DataProvider[String, String] = DataProviderUtil.withFilter[String, String](impl)

}

class RegisteredProvidersDataProvider(implicit bridge: BridgeClient) {

  private def count(filter: Option[String]): Int =
    bridge
      .listRegisteredProviders(0, 1000) //TODO remove HC limit
      .genericHandle()
      .map(_.size)
      .getOrElse(0)

  private def page(page: Int, offset: Int, filter: Option[String]): Seq[RegisteredProvider] = bridge.listRegisteredProviders(
    page,
    offset
  ).genericHandle()
    .getOrElse(Seq.empty)

  private lazy val impl = DataProviderUtil.createProviderSupport(page, count)

  def create: DataProvider[RegisteredProvider, String] = DataProviderUtil.withFilter[RegisteredProvider, String](impl)

}