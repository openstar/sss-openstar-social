package sss.openstar.ui.util

import com.vaadin.flow.component.UI
import com.vaadin.flow.component.html.Div
import com.vaadin.flow.component.orderedlayout.VerticalLayout

object ScrollTarget {

  private val defaultTargetName = "scrollToHere"

  def apply(height: String = "1px", targetName: String = defaultTargetName): ScrollTarget = new ScrollTarget(height, targetName)

  def scrollTo(targetName: String = defaultTargetName)(implicit ui: UI): Unit = {
    ui.getPage.executeJs(s"""document.getElementById("${targetName}").scrollIntoView();""")
  }
}

class ScrollTarget(height: String, targetName: String) extends VerticalLayout {
  setHeight(height)
  setSpacing(false)
  setPadding(false)
  val buffer = new Div()
  buffer.setHeight(height)
  val scrollTarget = new Div()
  add(buffer, scrollTarget)
  scrollTarget.getElement.setAttribute("id", targetName)

  def scrollTo()(implicit ui: UI): Unit = ScrollTarget.scrollTo(targetName)

}
