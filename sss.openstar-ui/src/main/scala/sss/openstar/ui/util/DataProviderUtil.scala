package sss.openstar.ui.util

import java.util

import com.vaadin.flow.data.provider.{DataProvider, Query}
import scala.jdk.CollectionConverters._

object DataProviderUtil {

  trait ProviderSupport[T] {
    def page(offset: Int, limit: Int, filter: Option[String]): Seq[T]
    def count(filter: Option[String]): Int
  }

  def createProviderSupport[T](p: (Int, Int, Option[String]) => Seq[T], c: Option[String] => Int): ProviderSupport[T] = new ProviderSupport[T] {
    override def page(offset: Int, limit: Int, filter: Option[String]): scala.Seq[T] = p(offset, limit, filter)
    override def count(filter: Option[String]): Int = c(filter)
  }

  def withFilter[T, String](impl: ProviderSupport[T]): DataProvider[T, String] = DataProvider.fromFilteringCallbacks(
    // First callback fetches items based on a query
    (query: Query[T, String]) => {
      def foo(query: Query[T, String]) = { // The index of the first item to load
        val offset: Int = query.getOffset
        val filter = queryToFilterOpt(query)
        // The number of items to load
        val limit: Int = query.getLimit
        val results: util.List[T] = impl.page(offset, limit, filter).asJava
        results.stream
      }

      foo(query)
    },
    // Second callback fetches the number of items for a query
    (query: Query[T, String]) => impl.count({
      queryToFilterOpt(query)
    })
  )

  private def queryToFilterOpt[String, T](query: _root_.com.vaadin.flow.data.provider.Query[T, String]) =  {
    if (query.getFilter.isPresent) {
      Option(query.getFilter.get()).map(_.toString)

    } else {
      None
    }
  }

  def apply[T](impl: ProviderSupport[T]): DataProvider[T, Void] = DataProvider.fromCallbacks(
    // First callback fetches items based on a query
    (query: Query[T, Void]) => {
      def foo(query: Query[T, Void]) = { // The index of the first item to load
        val offset: Int = query.getOffset
        // The number of items to load
        val limit: Int = query.getLimit
        val results: util.List[T] = impl.page(offset, limit, None).asJava

        results.stream
      }

      foo(query)
    },
    // Second callback fetches the number of items for a query
    (query: Query[T, Void]) => {
      impl.count(None)
    }
  )
}
