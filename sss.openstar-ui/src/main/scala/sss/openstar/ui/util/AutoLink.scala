package sss.openstar.ui.util

object AutoLink {

  import java.util.regex.Pattern

  private val httpLinkPattern: Pattern = Pattern.compile("(http[s]?)://(www\\.)?([\\S&&[^.@]]+)(\\.[\\S&&[^@]]+)")

  private val wwwLinkPattern: Pattern = Pattern.compile("(?<!http[s]?://)(www\\.+)([\\S&&[^.@]]+)(\\.[\\S&&[^@]]+)")

  private val mailAddressPattern: Pattern = Pattern.compile("[\\S&&[^@]]+@([\\S&&[^.@]]+)(\\.[\\S&&[^@]]+)")

  def autoLink(stringNoHtml: String) : String = {
    val httpLinksMatcher = httpLinkPattern.matcher(stringNoHtml)
    var textWithHttpLinksEnabled = httpLinksMatcher.replaceAll("<a href=\"$0\" target=\"_blank\">$0</a>")
    val wwwLinksMatcher = wwwLinkPattern.matcher(textWithHttpLinksEnabled)
    textWithHttpLinksEnabled = wwwLinksMatcher.replaceAll("<a href=\"http://$0\" target=\"_blank\">$0</a>")
    val mailLinksMatcher = mailAddressPattern.matcher(textWithHttpLinksEnabled)
    mailLinksMatcher.replaceAll("<a href=\"mailto:$0\">$0</a>")
  }
}
