package sss.openstar.ui

import com.vaadin.flow.component.UI
import sss.openstar.ui.rpc.MessagePayloadType
import sss.openstar.ui.views.{Create, InBox, MessageDetail, Profile}

object Helper  {


  def gotoMessageDetail(index: Long): Unit = UI.getCurrent.navigate(classOf[MessageDetail], new java.lang.Long(index))

  def gotoProfile(): Unit = UI.getCurrent.navigate(classOf[Profile])

  def gotoInbox(): Unit = UI.getCurrent.navigate(classOf[InBox])

  def gotoNewMessage(): Unit =
    UI.getCurrent.navigate(classOf[Create],
      new java.lang.Integer(MessagePayloadType.EncryptedMessageType.id))


  def refreshUserAvatar(): Unit = MainLayout.get.getAppBar.initAvatar()

}
