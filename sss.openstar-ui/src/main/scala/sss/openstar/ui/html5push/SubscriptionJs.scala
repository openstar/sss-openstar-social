package sss.openstar.ui.html5push

import scala.concurrent.{Future, Promise}
import scala.util.Try

import com.vaadin.flow.component.UI
import com.vaadin.flow.component.dependency.JavaScript
import com.vaadin.flow.component.page.Page
import com.vaadin.flow.internal.JsonSerializer
import elemental.json.JsonString
import sss.openstar.ui.JsLocation
import sss.openstar.ui.views.support.PushSupport


class SubscriptionJs(page: Page) {
  page.addJavaScript(s"context://${JsLocation.folder}/registerForPush.js")


  def getSubscription(implicit ui: UI): Future[Subscription] = {
    val promise = Promise[Subscription]()
    PushSupport.push {
      page.executeJs("return getSubscription()").`then`({ x =>
        val tried = Try(JsonSerializer.toObject(classOf[Subscription], x))
        promise.complete(tried)
      })
    }
    promise.future
  }


}