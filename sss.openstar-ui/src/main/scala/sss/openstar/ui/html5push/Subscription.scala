package sss.openstar.ui.html5push

import scala.beans.BeanProperty

class Subscription() {

  @BeanProperty
  var endpoint: String = ""

  @BeanProperty
  var auth: String = ""

  @BeanProperty
  var key: String = ""

}
