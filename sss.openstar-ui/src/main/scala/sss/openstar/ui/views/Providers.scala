package sss.openstar.ui.views

import scala.util.Try
import com.vaadin.flow.component.button.Button
import com.vaadin.flow.component.combobox.ComboBox
import com.vaadin.flow.component.icon.VaadinIcon
import com.vaadin.flow.component.orderedlayout.{FlexComponent, HorizontalLayout, VerticalLayout}
import com.vaadin.flow.component.textfield.TextField
import com.vaadin.flow.component.{Component, HasSize, ItemLabelGenerator}
import com.vaadin.flow.data.value.ValueChangeMode
import com.vaadin.flow.router.{PageTitle, Route}
import sss.openstar.Bridger._
import sss.openstar.ui.AppContext.bridge.{addProvider, listProviders, maxCallWaitDuration, removeProvider}
import sss.openstar.ui.rpc.ImplicitOps._
import sss.openstar.ui.rpc._
import sss.openstar.ui.util.{IdentitiesDataProvider, RegisteredProvidersDataProvider}
import sss.openstar.ui.views.Providers.{RegisteredProviderLabelGenerator, StoreHolder}
import sss.openstar.ui.views.support.{CurrentUISupport, PushSupport, RequireLoginSupportImpl, SessionIdSupport}
import sss.openstar.ui.{AppContext, MainLayout}


object Providers {
  case class StoreHolder(l: HorizontalLayout, b: Button, t: Component)

  object RegisteredProviderLabelGenerator extends ItemLabelGenerator[RegisteredProvider] {
    override def apply(t: RegisteredProvider): String = s"${t.name} (charges ${t.amount})"
  }
}

@Route(value = "providers", layout = classOf[MainLayout])
@PageTitle("Providers")
class Providers() extends ViewFrame
  with SessionIdSupport
  with CurrentUISupport
  with PushSupport
  with RequireLoginSupportImpl {

  addClassName("centered-layout")

  private val registeredUsersDataProvider: RegisteredProvidersDataProvider = AppContext.registeredProvidersDataProvider
  private val bridge: BridgeClient =  AppContext.bridge

  def makeStoreHolder(middleComponent: Component with HasSize): StoreHolder = {
    val hl = new HorizontalLayout()
    hl.setAlignItems(FlexComponent.Alignment.BASELINE)
    hl.setWidth("100%")
    val btn = new Button()
    btn.setIcon(VaadinIcon.TRASH.create())
    //btn.getS.addStyleNames("inline-icon")
    middleComponent.setWidth("100%")
    hl.add(middleComponent, btn)
    StoreHolder(hl, btn, middleComponent)
  }

  private val providersLayout = new VerticalLayout()

  def drawProviderLayouts: Seq[HorizontalLayout] = {

    var index = 0

    listProviders(sessId.value).genericHandle().map (stores => {
      stores map { store =>
        val tf = new TextField()
        val text = s"${store.name} (${store.amount})"
        tf.setValue(text)
        tf.setReadOnly(true)
        tf.setLabel("Current provider")
        val holder = makeStoreHolder(tf)

        providersLayout.addComponentAtIndex(index, holder.l)
        index += 1

        holder.b.addClickListener { _ =>
          removeProvider(sessId.value, store.name).await.genericHandle().foreach { _ =>
            push {
              providerLayouts.foreach(providersLayout.remove(_))
              providerLayouts = drawProviderLayouts

            }
          }
        }
        holder.l
      }
    })
  }.getOrElse(Seq.empty)

  private var providerLayouts: Seq[HorizontalLayout] = drawProviderLayouts

  private val comboBox = new ComboBox[RegisteredProvider]()
  comboBox.setLabel("Add another provider")
  comboBox.setItemLabelGenerator(RegisteredProviderLabelGenerator)

  comboBox.setDataProvider(registeredUsersDataProvider.create)
  //comboBox.setEmptySelectionAllowed(false)

  private val newProvider = makeStoreHolder(comboBox)

  newProvider.b.setText("Add")
  newProvider.b.setIcon(null)

  newProvider.b.addClickListener(e => {
    addProvider(sessId.value, comboBox.getValue.name).await.genericHandle().foreach { _ =>
      push {
        providerLayouts.foreach(providersLayout.remove(_))
        providerLayouts = drawProviderLayouts

      }
    }
  })

  val chargeLayoutOpt: Option[HorizontalLayout] = bridge.getProviderCharge(sessId.value).genericHandle()
    .flatMap {
      case None => None
      case Some(chargeOpt) =>

        val chargeAmountTf = new TextField()
        chargeAmountTf.setValueChangeMode(ValueChangeMode.EAGER)

        def makeSetValueParam(): Option[Long] = {
          if(chargeAmountTf.getValue.isEmpty) None
          else Some(chargeAmountTf.getValue.toLong)
        }


        val okBtn = new Button("Ok")
        okBtn.setEnabled(false)
        chargeOpt.foreach(amount => chargeAmountTf.setValue(amount.toString))
        val providerCharge = new HorizontalLayout()
        providerCharge.add(chargeAmountTf, okBtn)

        chargeAmountTf.addValueChangeListener(e => {
          if(e.getOldValue != e.getValue) {
            val change = e.getHasValue
            val validates = Try(change.getValue.toLong).map(_ > 0).getOrElse(false)
            if (change.isEmpty) {
              okBtn.setEnabled(true)
              okBtn.setText("I don't want to be a provider")
            } else if (validates) {
              okBtn.setEnabled(true)
              okBtn.setText("Set amount provider charges")
            } else {
              okBtn.setEnabled(false)
            }
          }
        })

        okBtn.addClickListener(_ => {
          bridge.setProviderCharge(sessId.value, makeSetValueParam()).genericHandle()
        })


        Some(providerCharge)

    }


  chargeLayoutOpt.map(l => providersLayout.add(l))

  providersLayout.add(providerLayouts: _*)
  providersLayout.add(newProvider.l)


  setViewContent(providersLayout)

}

