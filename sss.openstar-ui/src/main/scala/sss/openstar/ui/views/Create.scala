package sss.openstar.ui.views


import com.vaadin.flow.component.button.Button
import com.vaadin.flow.component.{AttachEvent, ClickEvent, UI}
import com.vaadin.flow.router._
import sss.openstar.ui.components.navigation.bar.BaseAppBar
import sss.openstar.ui.views.support.{RequireLoginSupportImpl, SessionIdSupport}
import sss.openstar.ui.{AppContext, Helper, MainLayout}


@Route(value = "create", layout = classOf[MainLayout])
@PageTitle("")
class Create extends ViewFrame
  with HasUrlParameter[java.lang.Integer]
  with SessionIdSupport
  with RequireLoginSupportImpl
{

  // The value can be retrieved as HTML
  override protected def onAttach(attachEvent: AttachEvent): Unit = {
    super.onAttach(attachEvent)

    appBar

  }

  private def render(msgType: Int) = {
    AppContext.renderer.catalog.find(_.messagePayloadType.id == msgType) match {
      case None =>
        appBar.setTitle(s"Unknown($msgType)")
      case Some(c) =>
        val content = AppContext.renderer.createNewMsgComponent(c.messagePayloadType,
          () =>  Helper.gotoInbox())
        setViewContent(content)
    }
  }

  private lazy val appBar = {
    val appBar = MainLayout.get.getAppBar
    appBar.setNaviMode(BaseAppBar.NaviMode.CONTEXTUAL)
    appBar.getContextIcon.addClickListener((e: ClickEvent[Button]) => UI.getCurrent.navigate(classOf[InBox]))
    appBar
  }

  override def setParameter(beforeEvent: BeforeEvent, payloadType: java.lang.Integer): Unit = {
    render(payloadType)
  }
}
