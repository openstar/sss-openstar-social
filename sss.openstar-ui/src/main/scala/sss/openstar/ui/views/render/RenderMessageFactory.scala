package sss.openstar.ui.views.render

import com.vaadin.flow.component.Component
import sss.openstar.ui.AppContext
import sss.openstar.ui.rpc.{DetailedDisplayMessage, SessionId}
import sss.openstar.ui.rpc.MessagePayloadType.MessagePayloadType
import sss.openstar.ui.views.render.RenderMessageFactory.OnDone
import sss.openstar.ui.views.render.RenderMessageFactory.ViewType.ViewType


trait MessageRenderer[M <: DetailedDisplayMessage] {
  def render(detailedDisplayMessage: M): Option[Component]
}

case class NullMessageRenderer[M <: DetailedDisplayMessage]() extends MessageRenderer[M] {
  def render(detailedDisplayMessage: M): Option[Component] = None
}

case class SimpleMessageRenderer[M <: DetailedDisplayMessage](f: M => Option[Component]) extends MessageRenderer[M] {
  override def render(detailedDisplayMessage: M): Option[Component] = f(detailedDisplayMessage)
}

trait RenderMessageFactory[M <: DetailedDisplayMessage] {

  val name: String
  val description: String
  val payloadType: MessagePayloadType

  def createNewWriteComponent(
                              onDone: OnDone
                             )(implicit appContext: AppContext, sessId: SessionId): Component

  def createRenderer(
             viewType: ViewType
             )(implicit appContext: AppContext, sessId: SessionId): MessageRenderer[M]

}

object RenderMessageFactory {

  type OnDone = () => Unit

  object ViewType extends Enumeration {
    type ViewType = Value
    val New = Value(0)
    val Sent = Value(1)
    val Archive = Value(2)
    val Junk = Value(3)
  }


}