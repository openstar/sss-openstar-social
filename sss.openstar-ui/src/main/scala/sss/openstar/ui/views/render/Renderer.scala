package sss.openstar.ui.views.render

import com.vaadin.flow.component.Component
import sss.openstar.ui.AppContext
import sss.openstar.ui.rpc.MessagePayloadType.MessagePayloadType
import sss.openstar.ui.rpc.{DetailedDisplayMessage, SessionId}
import sss.openstar.ui.views.render.RenderMessageFactory.OnDone
import sss.openstar.ui.views.render.RenderMessageFactory.ViewType.ViewType
import sss.openstar.ui.views.render.Renderer.Catalog

import scala.jdk.CollectionConverters._

object Renderer {

  case class Catalog(name: String, description: String, messagePayloadType: MessagePayloadType)

}

class Renderer(renderers: RenderMessageFactory[_ <: DetailedDisplayMessage]*) {

  val catalog: Seq[Catalog] = renderers.map(renderer =>
    Catalog(renderer.name, renderer.description, renderer.payloadType)
  )

  lazy val jCatalog: java.util.Collection[Catalog] = catalog.asJavaCollection

  private val renderersMap:Map[MessagePayloadType, RenderMessageFactory[_]]
  = renderers.map(renderer => renderer.payloadType -> renderer).toMap

  def createNewMsgComponent(
                             messagePayloadType: MessagePayloadType,
                             onDone: OnDone)(implicit sessionId: SessionId,
                                             appContext: AppContext): Component =

    renderersMap(messagePayloadType).createNewWriteComponent(
      onDone
    )


  def apply[M <: DetailedDisplayMessage](
                                          msg: M,
                                          viewType: ViewType
                                        )(implicit appContext: AppContext, sessionId: SessionId): MessageRenderer[M] = {

    renderersMap(msg.msgPayloadType).createRenderer(
      viewType
    ).asInstanceOf[MessageRenderer[M]]
  }

}
