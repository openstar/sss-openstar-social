package sss.openstar.ui.views

import java.io.ByteArrayInputStream

import scala.util.{Failure, Success, Try}
import java.io.ByteArrayInputStream

import com.vaadin.flow.component.{Component, ComponentEvent}
import com.vaadin.flow.component.button.{Button, ButtonVariant}
import com.vaadin.flow.component.html.{Div, Span}
import com.vaadin.flow.component.icon.VaadinIcon
import com.vaadin.flow.component.orderedlayout.{FlexLayout, HorizontalLayout}
import com.vaadin.flow.component.upload.receivers.MemoryBuffer
import com.vaadin.flow.component.upload.{SucceededEvent, Upload}
import com.vaadin.flow.dom.DomEventListener
import com.vaadin.flow.router.{PageTitle, Route}
import com.vaadin.flow.server.{InputStreamFactory, StreamResource}
import sss.openstar.Bridger._
import sss.openstar.ui.rpc._
import sss.openstar.ui.util.ImageUtil
import sss.openstar.ui.views.support.{CurrentUISupport, PushSupport, RequireLoginSupportImpl, SessionIdSupport}
import sss.openstar.ui.views.support.NotificationSupport._
import sss.openstar.ui.{AppContext, BaseHelper, Helper, MainLayout}

import scala.util.{Failure, Success, Try}


object Profile {
  case class StoreHolder(l: HorizontalLayout, b: Button, t: Component)
}

@Route(value = "profile", layout = classOf[MainLayout])
@PageTitle("Profile")
class Profile() extends ViewFrame
  with SessionIdSupport
  with CurrentUISupport
  with PushSupport
  with BridgeLogging
  with RequireLoginSupportImpl {

  addClassNames("centered-layout", "align-center")
  addClassName("profile-view")

  private val bridge: BridgeClient =  AppContext.bridge

  private val buffer = new MemoryBuffer()
  private val upload = new Upload(buffer)

  private val image = BaseHelper.getUserAvatar
  private val originalImageSrc = image.getSrc

  private val saveBtn = new Button("Save changes")
  saveBtn.addThemeVariants(ButtonVariant.LUMO_PRIMARY)
  saveBtn.getStyle.set("margin-top", "16px")
  saveBtn.setEnabled(false)

  private val removeAvatarBtn = new Button("Remove Avatar")

  private val maxFileSizePreScaling = 1024 * 1500
  private val maxFileSizePostScaling = 1024 * 600

  private var resizedBufferOpt: Option[Array[Byte]] = None

  image.setMaxWidth("200px")

  upload.setDropLabel(new Span("Select file to replace current avatar"))
  upload.setUploadButton(VaadinIcon.PAPERCLIP.create())


  upload.addSucceededListener((event: SucceededEvent) => {

    if(event.getMIMEType.startsWith("image/")) {

      Try(
        ImageUtil.scaleImageInputStream(buffer.getInputStream)(100)(100)
      ) match {
        case Failure(e) =>
          showWarn(s"Failed to scale file, please remove it and try again.")
          log.warning(s"Could not resize profile image $e")

        case Success(arrayBytes) if (arrayBytes.length > maxFileSizePostScaling)=>
          showWarn(s"File is too big (> $maxFileSizePostScaling) post scaling... remove!")

        case Success(arrayBytes) =>

          //println(s"Content len is ${event.getContentLength} array size ${arrayBytes.size}")
          resizedBufferOpt = Option(arrayBytes)

          val streamFactory: InputStreamFactory = () => new ByteArrayInputStream(arrayBytes)
          val fileName = event.getFileName

          image.setSrc(new StreamResource(fileName, streamFactory))
          updateImageWrapper()
          saveBtn.setEnabled(true)
      }
    } else {
      showWarn(s"File mimetype is not 'image' (${event.getMIMEType}}")
    }
  })


  upload.addFailedListener(e => {
    showWarn(s"Upload failed, ${e.getReason}")
  })

  removeAvatarBtn.addClickListener(e => {
    bridge.setAvtar(sessId.value, None).genericHandle().map { _ =>
      Helper.refreshUserAvatar()
      Helper.gotoInbox()
    }
  })

  saveBtn.addClickListener(e => {
    resizedBufferOpt.foreach { resizedBuffer =>
      bridge.setAvtar(sessId.value, Some(resizedBuffer)).genericHandle().map { _ =>
          Helper.refreshUserAvatar()
          Helper.gotoInbox()
      }
    }
  })

  val imageWrapper = new Div(image)
  imageWrapper.addClassName("avatar-image")
  updateImageWrapper()

  def updateImageWrapper(): Unit = {
    imageWrapper.getStyle.set("background-image", s"url(${image.getSrc})")
  }

  val avatarActions = new FlexLayout(imageWrapper, upload, removeAvatarBtn, saveBtn)
  avatarActions.addClassName("avatar-actions")

  val imagePreviewLayout = new FlexLayout(avatarActions)
  imagePreviewLayout.setSizeFull()

  val fileRejectDomEventListener: DomEventListener = e => showWarn(s"Avatar image too big! Must be less than $maxFileSizePreScaling bytes")
  upload.getElement.addEventListener(
    "file-reject", fileRejectDomEventListener
  )

  val fileRemoveDomEventListener: DomEventListener = event => {
    image.setSrc(originalImageSrc)
    resizedBufferOpt = None
    updateImageWrapper()
    saveBtn.setEnabled(false)
  }

  upload.getElement.addEventListener("file-remove", fileRemoveDomEventListener)

  upload.setMaxFileSize(maxFileSizePreScaling)

  setViewContent(imagePreviewLayout)

}

