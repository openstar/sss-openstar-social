package sss.openstar.ui.views.support

import com.vaadin.flow.component.UI
import sss.openstar.ui.views.InBox

trait GoHomeSupport {

  def navigateHome(): Unit = UI.getCurrent.navigate(classOf[InBox])

}
