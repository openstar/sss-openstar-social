package sss.openstar.ui.views.support

import java.util.concurrent.atomic.AtomicReference

import com.google.protobuf.ByteString
import com.vaadin.flow.component.button.Button
import com.vaadin.flow.component.icon.VaadinIcon
import com.vaadin.flow.component.orderedlayout.{HorizontalLayout, ThemableLayout, VerticalLayout}
import com.vaadin.flow.component.textfield.TextArea
import com.vaadin.flow.component.upload.{SucceededEvent, Upload}
import com.vaadin.flow.component.{HasOrderedComponents, HasSize, HasStyle}
import com.vaadin.flow.dom.DomEventListener
import sss.ancillary.{Guid, Logging}
import sss.openstar.ui.AppContext.bridge.maxCallWaitDuration
import sss.openstar.ui.rpc.DecryptedMessage
import sss.openstar.ui.rpc.ImplicitOps.FutureOps
import sss.openstar.ui.util.CoinUtil._
import sss.openstar.ui.util.{AutoLink, RpcBridgeMultiFileReceiver, RpcBridgeMultiFileReceiverFactory}
import sss.openstar.ui.views.render.RenderMessageFactory.OnDone
import sss.openstar.ui.views.support.NotificationSupport.{show, showWarn}
import sss.openstar.Bridger.ProblemOps
import sss.openstar.ui.AppContext.bridge

trait WriteMessageSupport {

  self: Logging
    with SessionIdSupport
    with HasSize
    with HasOrderedComponents[_]
    with HasStyle
    with AttachmentRenderSupport
    with AppContextSupport
    with RecipientsCostSupport
    with ThemableLayout =>

  def validate(): Option[String] = {
    if (textArea.isEmpty && buffer.isEmpty) {
      Some("Put some text in the body or post a photo/file")
    } else None
  }


  val `1MB` = 1048576
  val maxFileSize: Int = 20000 * `1MB`
  val maxFileSizeHumanReadable = s"20G"

  val defaultNumBlockToLive = 168
  @volatile
  private var numBlocksToLive: Int = defaultNumBlockToLive

  def blocksToLive(): Int = synchronized(numBlocksToLive)

  def setNumBlocksToLive(num: Int): Unit = synchronized{numBlocksToLive = num}

  val onDone: OnDone

  val fileUploadBufferFactory = new RpcBridgeMultiFileReceiverFactory()

  private val parentId: AtomicReference[Option[Long]] = new AtomicReference(None)

  def setParentId(pId: Option[Long]): Unit = parentId.set(pId)

  def setTos(tos: Set[String]): Unit = {
    removeRecipients()
    tos.foreach { to =>
      costToContact(to).map(addRecipient(to, _))
    }
  }


  override def onBalanceExceeded(totalCost: Long, balance: Long): Unit = {
    showWarn(s"Total cost (${totalCost}) exceeds available balance ${balance}, get more starz")
    sendComp.sendButton.setEnabled(false)
  }

  override def onTotalCostChange(newCost: Long): Unit = {
    sendComp.updateAmount(newCost)
    sendComp.sendButton.setEnabled(true)
  }

  addClassName("msg-write-layout")
  setPadding(false)
  setSpacing(false)
  val textArea = new TextArea()

  textArea.setPlaceholder("Your message")
  textArea.setSizeFull()


  /**
   * Hack required as upload cannont be reset.
   */
  private def resetUploadBuffers() = {
    buffer = fileUploadBufferFactory.forGuid(Guid())
    val newUpload = new Upload(buffer)

    Option(upload).foreach(replace(_, newUpload))

    upload = newUpload
    upload.setUploadButton(uploadBtn)
    upload.setDropAllowed(false)

    upload.setMaxFileSize(maxFileSize)


    val fileRemoveDomEventListener: DomEventListener =event => {
      val eventData = event.getEventData
      val name = eventData.getString("event.detail.file.name")

      buffer.remove(name)
      removeAttachmentComponent(name, imagePreviewLayout)

    }
    upload.getElement.addEventListener("file-remove", fileRemoveDomEventListener).addEventData("event.detail.file.name")

    upload.addStartedListener(e => {
      //prevent user pressing 'send'
      sendComp.sendButton.setEnabled(false)
    })

    upload.addAllFinishedListener(e => {
      sendComp.sendButton.setEnabled(true)
    })

    upload.addSucceededListener((event: SucceededEvent) => {

      val newC =
        createAttachmentComponent(
          event.getFileName,
          event.getContentLength,
          event.getMIMEType,
          () => buffer.getInputStream(event.getFileName)
        )


      removeAttachmentComponent(event.getFileName, imagePreviewLayout)
      imagePreviewLayout.add(newC)


    })

    val fileRejectDomEventListener: DomEventListener = _ => showWarn(s"Upload failed, file must be less than $maxFileSizeHumanReadable")

    upload.getElement.addEventListener("file-reject", fileRejectDomEventListener)
    upload.addFailedListener(e => {
      sendComp.sendButton.setEnabled(true)
      showWarn(s"Upload failed, ${e.getReason}")
    })

    upload.setWidthFull()
  }

  private var buffer: RpcBridgeMultiFileReceiver = _
  private var upload: Upload = _

  private val uploadBtn = new Button(VaadinIcon.PAPERCLIP.create())
  uploadBtn.setText("Attach")

  uploadBtn.getElement().setAttribute("theme", "responsive-icon")

  resetUploadBuffers()


  val sendComp = createCoinSendComponent()

  //sendComp.div.add(upload)

  val writeMsgWrapper = new HorizontalLayout()
  //writeMsgWrapper.addClassName("msg-write-wrapper")
  writeMsgWrapper.setWidthFull()
  writeMsgWrapper.add(textArea, sendComp.div)
  writeMsgWrapper.getStyle.set("margin-bottom", "4px")
  add(writeMsgWrapper)

  val imagePreviewLayout = new VerticalLayout()
  imagePreviewLayout.setClassName("msg-attachment")
  imagePreviewLayout.setPadding(false)


  sendComp.sendButton.addClickListener(_ => {
    validate() match {
      case Some(error) => show(error)
      case None =>
        val text = AutoLink.autoLink(textArea.getValue)

        val amountPerRecipient = getAmountPerRecipient()
        require(amountPerRecipient.toInt == amountPerRecipient)
        val minAmount = if (amountPerRecipient < 1) 0 else amountPerRecipient.toInt

        val parentOpt = parentId.get()
        val enc = DecryptedMessage(text, minAmount, recipients.toSeq, blocksToLive(), parentOpt, ByteString.copyFrom(buffer.guid.value))

        parentOpt.foreach(id => appContext.displayMsgCache.invalidate(sessId, id))

        val r = appContext.bridge.sendEncryptedMessage(sessId.value, enc)
        r.await.genericHandle().map { _ =>
          textArea.setValue("")
          removeAttachmentComponents(imagePreviewLayout)
          resetUploadBuffers()
          onDone()
        }

    }
  })

  add(upload, imagePreviewLayout)

}
