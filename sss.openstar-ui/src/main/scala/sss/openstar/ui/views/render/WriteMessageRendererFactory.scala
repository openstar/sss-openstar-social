package sss.openstar.ui.views.render

import com.vaadin.flow.component.Component
import sss.openstar.ui.AppContext
import sss.openstar.ui.rpc.{DetailedEncryptedMessage, MessagePayloadType, SessionId}
import sss.openstar.ui.rpc.MessagePayloadType.MessagePayloadType
import sss.openstar.ui.views.{ViewMessage, WriteMessage}
import sss.openstar.ui.views.render.RenderMessageFactory.OnDone
import sss.openstar.ui.views.render.RenderMessageFactory.ViewType.ViewType

class WriteMessageRendererFactory extends RenderMessageFactory[DetailedEncryptedMessage] {

  val name: String = "New Message"
  val description: String = "Create Message with embedded credits"
  val payloadType: MessagePayloadType = MessagePayloadType.EncryptedMessageType

  def createNewWriteComponent(
                               onDone: OnDone
                             )(implicit appContext: AppContext, sessId: SessionId): Component = {

    new WriteMessage(onDone)
  }

  def createRenderer(
              viewType: ViewType
            )(implicit appContext: AppContext, sessId: SessionId): MessageRenderer[DetailedEncryptedMessage] = {

           new ViewMessage(viewType)

  }


}
