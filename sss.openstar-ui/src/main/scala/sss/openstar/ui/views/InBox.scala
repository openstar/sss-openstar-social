package sss.openstar.ui.views


import scala.util.{Success, Try}
import com.vaadin.flow.component.button.{Button, ButtonVariant}
import com.vaadin.flow.component.grid.contextmenu.GridContextMenu.GridContextMenuItemClickEvent
import com.vaadin.flow.component.grid.{ColumnTextAlign, Grid}
import com.vaadin.flow.component.html.{Div, Image}
import com.vaadin.flow.component.icon.VaadinIcon
import com.vaadin.flow.component.orderedlayout.{FlexComponent, FlexLayout, HorizontalLayout, VerticalLayout}
import com.vaadin.flow.component._
import com.vaadin.flow.data.renderer.ComponentRenderer
import com.vaadin.flow.data.selection.SelectionEvent
import com.vaadin.flow.router._
import sss.ancillary.Logging
import sss.openstar.Bridger._
import sss.openstar.ui.components.FlexBoxLayout
import sss.openstar.ui.rpc.ImplicitOps._
import sss.openstar.ui.rpc.SessionPushEventManager.{NewMessagePushEvent, PushEvent, RegistrationId}
import sss.openstar.ui.rpc.{DisplayMessage, SessionPushEventManager}
import sss.openstar.ui.util._
import sss.openstar.ui.views.support.NotificationSupport._
import sss.openstar.ui.views.support._
import sss.openstar.ui.{AppContext, Helper, MainLayout}


@Route(value = "inbox", layout = classOf[MainLayout])
@PageTitle("In Box")
class InBox extends ViewFrame
  with RequireLoginSupportImpl
  with SessionIdSupport
  with UserIdSupport
  with BeforeLeaveObserver
  with AfterNavigationObserver
  with Logging {

  private val grid = new Grid[DisplayMessage](8)
  private lazy val dataProvider = AppContext.displayMessageDataProvider.create

  private var registrationNewMsg: Option[RegistrationId] = None

  val contextMenu = grid.addContextMenu()

  val deleteHandler: ComponentEventListener[GridContextMenuItemClickEvent[DisplayMessage]] = ev => {
    Try(ev.getItem.get) match {
      case Success(item: DisplayMessage) =>
        AppContext.displayMsgCache.invalidate(sessId, item.id)
        AppContext.bridge.deleteMessage(sessId.value, item.id).genericHandle()
          .foreach { _ =>
            dataProvider.refreshAll()
          }
      case _ =>
        showWarn("Failed to delete message!")
    }
  }


  contextMenu.addItem("Delete", deleteHandler)

  setViewContent(createContent)


  override def afterNavigation(event: AfterNavigationEvent): Unit = {
    MainLayout.get().getAppBar.reset()
  }

  private def createContent = {
    val content = new VerticalLayout()
    content.setSpacing(false)
    content.addClassName("grid-view")

    val newMsgBtn = new Button("New message")
    newMsgBtn.addThemeVariants(ButtonVariant.LUMO_PRIMARY)
    newMsgBtn.addClickListener(e => Helper.gotoNewMessage())

    content.add(newMsgBtn, createGrid)
    content
  }


  private def createGrid = {


    grid.addSelectionListener(
      (event: SelectionEvent[Grid[DisplayMessage], DisplayMessage]) => event.getFirstSelectedItem.ifPresent(this.viewDetails))
    grid.setDataProvider(dataProvider)
    grid.setId("inbox")
    grid.setSizeFull()

    val amountColHeader = UIUtils.createIcon(IconSize.S, TextColor.TERTIARY, VaadinIcon.COIN_PILES)

    val div = new Div(amountColHeader)
    div.getStyle.set("text-align", "center")

    val amountRenderer = new ComponentRenderer[Component, DisplayMessage](a => createAmount(a))
    val avatarRenderer = new ComponentRenderer[Component, DisplayMessage](a => createAvatar(a))
    val subjectAuthorDateRenderer = new ComponentRenderer[Component, DisplayMessage](a => createSubjectDateAuthor(a))


    grid.addColumn(amountRenderer)
      .setHeader(div)
      .setFlexGrow(0)
      .setAutoWidth(true)
      .setTextAlign(ColumnTextAlign.END)


    grid.addColumn(avatarRenderer)
      .setHeader(createHeader("Author"))
      .setFlexGrow(0)
      .setAutoWidth(true)



    grid.addColumn(subjectAuthorDateRenderer)
      .setHeader(createHeader("Details"))
      .setFlexGrow(1)
      //.setAutoWidth(true) Don't do it, it causes problems with the elipsis

    grid
  }

  private def createHeader(header: String) = {
    header
  }

  private def createAmount(dm: DisplayMessage) = {
   CoinUtil.createLabel(dm.amount.getOrElse(0), dm.author != userId.value)
  }

  private def createSubjectDateAuthor(dm: DisplayMessage) = {

    val authorLabel = UIUtils.createLabel(FontSize.S, dm.author.replaceAll("_", " "))
    UIUtils.setFontWeight(FontWeight.BOLD, authorLabel)
    val receivedAt = UIUtils.createLabel(FontSize.XS, FormatUtil.toHumanReadableString(dm.receivedAt))
    UIUtils.setFontWeight(FontWeight.LIGHTER, receivedAt)


    val bannerLayout = new HorizontalLayout(authorLabel, receivedAt)

    bannerLayout.setSpacing(false)
    bannerLayout.setPadding(false)
    bannerLayout.setWidthFull()
    bannerLayout.setFlexGrow(1, authorLabel)
    bannerLayout.setFlexGrow(0, receivedAt)


    val subjectLabel = UIUtils.createLabel(FontSize.S, dm.subject)
    subjectLabel.setWidthFull()
    subjectLabel.addClassName("label-ellipsis")

    val layout = new VerticalLayout(bannerLayout, subjectLabel)
    layout.setSpacing(false)
    layout.setPadding(false)

    layout.addClickListener(e => {
      viewDetails(dm)
    })

    layout
  }

  private def createAvatar(dm: DisplayMessage) = {

    val image: Image = dm.avatar
    image.addClassName("avatar")
    val flexLayout: FlexLayout = new FlexBoxLayout(image)
    flexLayout.setAlignItems(FlexComponent.Alignment.CENTER)
    flexLayout
  }


  private def viewDetails(dm: DisplayMessage): Unit = {
    Helper.gotoMessageDetail(dm.id)
  }

  override protected def onAttach(attachEvent: AttachEvent): Unit = {
    //registerForNewMessages(attachEvent.getUI)

    super.onAttach(attachEvent)
  }

  override def onDetach(detachEvent: DetachEvent): Unit = {

    //deRegisterForNewMessages(detachEvent.getUI)
    super.onDetach(detachEvent)
  }

  def deRegisterForNewMessages(ui: UI): Unit = {
    registrationNewMsg.foreach( regId =>
      SessionTracker.eventManagerOpt(ui).map(_.deRegister(regId))
    )
    registrationNewMsg = None
  }

  override def beforeLeave(event: BeforeLeaveEvent): Unit = {
    deRegisterForNewMessages(event.getUI)
  }


  private def registerForNewMessages(implicit ui: UI): Unit = {

    def onMessage(ev: PushEvent): Unit = {

      ev match {
        case NewMessagePushEvent(msgId) =>
          PushSupport.push({
            dataProvider.refreshAll()
          })
        case x =>
      }
    }

    if (registrationNewMsg.isEmpty) {
      registrationNewMsg =
        SessionTracker.eventManagerOpt(ui).map(_.register(SessionPushEventManager.PushEventType.NewMsgType, onMessage))
    }
  }

  override def beforeEnterPostLogin(beforeEnterEvent: BeforeEnterEvent): Unit = {

    registerForNewMessages(beforeEnterEvent.getUI)
  }
}
