package sss.openstar.ui.views


import com.vaadin.flow.component.{AttachEvent, UI}
import com.vaadin.flow.component.button.Button
import com.vaadin.flow.component.orderedlayout.VerticalLayout
import com.vaadin.flow.router.{BeforeEnterEvent, BeforeEnterObserver, PageTitle, Route}
import sss.openstar.ui.{BaseHelper, Helper, MainLayout}


@Route(value = "logout", layout = classOf[MainLayout])
@PageTitle("Logout")
class Logout extends ViewFrame
   with BeforeEnterObserver
{

  override def beforeEnter(event: BeforeEnterEvent): Unit = {
    BaseHelper.endSession()
    //event.rerouteTo(classOf[Login])
    //UI.getCurrent.navigate(classOf[Login])
    event.forwardTo(classOf[Login])
  }

  val btn = new Button()

  btn.addClickListener(e => {
    UI.getCurrent.navigate(classOf[Login])
  })

  setViewContent(btn)

}


