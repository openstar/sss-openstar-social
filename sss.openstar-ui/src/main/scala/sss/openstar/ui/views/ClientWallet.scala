package sss.openstar.ui.views

import com.vaadin.flow.component.button.Button
import com.vaadin.flow.component.icon.VaadinIcon
import com.vaadin.flow.component.orderedlayout.{HorizontalLayout, VerticalLayout}
import com.vaadin.flow.component.textfield.TextField
import com.vaadin.flow.component.{AttachEvent, Component, DetachEvent, UI}
import com.vaadin.flow.router._
import sss.openstar.Bridger.ProblemOps
import sss.openstar.ui.{AppContext, MainLayout}
import sss.openstar.ui.rpc.SessionPushEventManager._
import sss.openstar.ui.rpc.{ClientBalance, SessionId}
import sss.openstar.ui.util.SessionTracker
import sss.openstar.ui.views.ClientWallet.{ClientBalanceLayout, RootLayout}
import sss.openstar.ui.views.support.{CurrentUISupport, PushSupport, RequireLoginSupportImpl, SessionIdSupport}

import scala.concurrent.ExecutionContext.Implicits.global

object ClientWallet {


  trait HasId {
    def getMyId: String
  }

  type ComponentEventConsumer = Component with PushEventConsumer
  type ComponentEventConsumerId = Component with PushEventConsumer with HasId


  case class RootLayout(layout: VerticalLayout) extends PushEventConsumer {

    var subsumers: Map[String, ComponentEventConsumerId] = Map.empty

    override def apply(ev : PushEvent): Unit = ev match {
      case newBal: NewClientBalancePushEvent if subsumers.contains(newBal.client) =>
        subsumers(newBal.client).apply(newBal)
      case _ =>
    }

    def clear(): Unit = {
      subsumers.values.foreach(sub => layout.remove(sub))
      subsumers = Map.empty
    }

    def setSubSumers(sumers:Seq[ComponentEventConsumerId]):Unit = {
      subsumers = sumers.map(s => (s.getMyId -> s)).toMap
      layout.add(sumers: _*)
    }
  }


  case class ClientBalanceLayout(clientBalance: ClientBalance)(implicit ui: UI, sessId: SessionId)
    extends HorizontalLayout with PushEventConsumer with HasId {
    override def getMyId: String = clientBalance.client

    private val myClientId = getMyId
    private val clientNameField = new TextField()
    clientNameField.setValue(myClientId)
    clientNameField.setReadOnly(true)

    private val clientBalanceField = new TextField()
    clientBalanceField.setValue(clientBalance.balance.toString)
    clientBalanceField.setReadOnly(true)

    private val refreshBtn = new Button()
    refreshBtn.setIcon(VaadinIcon.REFRESH.create())

    refreshBtn.addClickListener(ev => {
      AppContext.bridge
        .clientBalance(sessId, myClientId)
        .genericHandle().map { bal  =>
      PushSupport.push(
        clientBalanceField.setValue(bal.toString)
      )
      }
    })
    add(clientNameField, clientBalanceField, refreshBtn)

    override def apply(ev: PushEvent): Unit = ev match {
      case NewClientBalancePushEvent(bal, `myClientId`, _) =>
          PushSupport.push {
            clientBalanceField.setValue(bal.toString)
          }
      case _ =>

    }
}
}
@Route(value = "clientwallet", layout = classOf[MainLayout])
@PageTitle("Client Wallet")
class ClientWallet() extends ViewFrame
  with SessionIdSupport
  with RequireLoginSupportImpl
  with AfterNavigationObserver
  with BeforeLeaveObserver {


  private implicit var ui: UI = _

  override def onAttach(attachEvent: AttachEvent): Unit = {
    ui = attachEvent.getUI
    super.onAttach(attachEvent)
  }

  override def onDetach(detachEvent: DetachEvent): Unit = {
    super.onDetach(detachEvent)
    balanceRegistrationId.foreach { regId =>
      SessionTracker.eventManagerOpt(ui).map(_.deRegister(regId))
    }
  }

  addClassName("centered-layout")

  override def beforeLeave(event: BeforeLeaveEvent): Unit = {
    balanceRegistrationId.foreach { regId =>
      SessionTracker.eventManagerOpt(ui).map(_.deRegister(regId))
    }
  }

  private var balanceRegistrationId: Option[RegistrationId] = None

  override def afterNavigation(event: AfterNavigationEvent): Unit = {

    balanceRegistrationId.foreach { regId =>
      SessionTracker.eventManagerOpt(ui).map(_.deRegister(regId))
    }

    balanceRegistrationId =
      SessionTracker
        .eventManagerOpt(ui)
        .map(_.register(PushEventType.NewClientBalanceType, root))


    AppContext.bridge.listClientBalances(sessId, 0, 50).map { result =>
      result.genericHandle().map { seqBalances =>

        val balanceLayouts = seqBalances.map(ClientBalanceLayout.apply)
        PushSupport.push {
          root.clear()
          root.setSubSumers(balanceLayouts)
        }
      }
    }
  }

  private val root = RootLayout(new VerticalLayout())

  setViewContent(root.layout)

}

