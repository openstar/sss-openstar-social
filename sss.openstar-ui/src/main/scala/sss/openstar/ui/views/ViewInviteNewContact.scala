package sss.openstar.ui.views

//import scala.jdk.CollectionConverters._

import com.vaadin.flow.component.button.{Button, ButtonVariant}
import com.vaadin.flow.component.combobox.ComboBox
import com.vaadin.flow.component.html.{Div, H3, Image, Label}
import com.vaadin.flow.component.orderedlayout.{FlexComponent, FlexLayout, HorizontalLayout, VerticalLayout}
import sss.ancillary.Logging
import sss.openstar.Bridger._
import sss.openstar.ui.AppContext.bridge.{maxCallWaitDuration, sendNewContactMessage}
import sss.openstar.ui.{AppContext, BaseHelper, Helper}
import sss.openstar.ui.rpc.ImplicitOps._
import sss.openstar.ui.rpc.{DetailedDisplayMessage, DetailedNewContactMessage, NewContactMessage, PaywallCategory}
import sss.openstar.ui.views.support.{AppContextSupport, GoHomeSupport, NotificationSupport, SessionIdSupport, UserIdSupport}


class ViewInviteNewContact(detailedMsg: DetailedNewContactMessage)(implicit val appContext: AppContext)
  extends VerticalLayout

    with SessionIdSupport
    with Logging
    with AppContextSupport
    with UserIdSupport
    with GoHomeSupport
{

  addClassNames("centered-layout", "align-center")
  addClassName("view-invite-new-contact")

  detailedMsg.inviteePaywalls.paywallCategory match {
    case Some(PaywallCategory(category, amount, _)) =>

      val msgAuthorName = new Label()
      msgAuthorName.addClassName("avatar-name")

      val avatarImage = if (detailedMsg.author == userId.value) {
        msgAuthorName.setText(s"${detailedMsg.invitee.replaceAll("_", " ")}")
        decorateAvatar(BaseHelper.getAvatarOrDefault(detailedMsg.invitee))
      } else {
        msgAuthorName.setText(s"${detailedMsg.author.replaceAll("_", " ")}")
        detailedMsg.avatar
      }

      decorateAvatar(avatarImage)

      val infoAuthor = new Label()

      infoAuthor.setText(s"${detailedMsg.author} is now a contact of ${detailedMsg.invitee} " +
        s"in paywall category '${category}' meaning it currently costs ${detailedMsg.author} " +
        s"${amount} to send ${detailedMsg.invitee} a message. ")

      val infoInvitee  = new Label()

      infoInvitee.setText(s"${detailedMsg.invitee} is now a contact of ${detailedMsg.author} in paywall category '${detailedMsg.invitorCategory}' meaning it currently costs ${detailedMsg.invitee} ${detailedMsg.invitorCurrentAmount} to send ${detailedMsg.author} a message. " +
        s"This will be automatically calculated by the application when sending messages.")

      add(new H3("Request confirmed"), avatarImage, msgAuthorName, infoAuthor, infoInvitee)

    case None =>
      val seqPaywalls = detailedMsg.inviteePaywalls.paywallCategories.get.categories

      val msgAuthorName = new Label()
      msgAuthorName.addClassName("avatar-name")
      val info = new Label()
      val choseCategoryLayout = new HorizontalLayout()
      choseCategoryLayout.setAlignItems(FlexComponent.Alignment.BASELINE)
      choseCategoryLayout.setWidthFull()

      val avatarImage = if(detailedMsg.author == userId.value) {
        msgAuthorName.setText(s"${detailedMsg.invitee.replaceAll("_", " ")}")
        info.setText(s"Awaiting response ...")
        decorateAvatar(BaseHelper.getAvatarOrDefault(detailedMsg.invitee))
      } else {
        msgAuthorName.setText(s"""${detailedMsg.author.replaceAll("_", " ")}""")
        val avatarImage = detailedMsg.avatar

        info.setText(s"${detailedMsg.author} is offering ${detailedMsg.invitee} paywall category '${detailedMsg.invitorCategory}' " +
          s"meaning it would require ${detailedMsg.invitorCurrentAmount} to send ${detailedMsg.author} a message. " +
          s"This will be automatically calculated by the application when sending messages. " +
          s"Please choose a paywall category to assign to ${detailedMsg.author} to make ${detailedMsg.author} a contact.")

        val choosePaywallCategoryCombo = new ComboBox[PaywallCategory]
        choosePaywallCategoryCombo.setLabel("Paywall category")
        choosePaywallCategoryCombo.getStyle().set("flex-grow", "1")
        choosePaywallCategoryCombo.setItems(seqPaywalls: _*)
        choosePaywallCategoryCombo.setAllowCustomValue(false)
        choosePaywallCategoryCombo.setPreventInvalidInput(true)
        choosePaywallCategoryCombo.setItemLabelGenerator(pw =>
          s"${pw.category} (Current Cost: ${pw.amount})"
        )

        val ignoreBtn = new Button("Ignore")
        ignoreBtn.addThemeVariants(ButtonVariant.LUMO_ERROR)

        ignoreBtn.addClickListener(_ => navigateHome())

        val acceptBtn = new Button("Accept")
        acceptBtn.setEnabled(false)

        choosePaywallCategoryCombo.addValueChangeListener(ev => {
          acceptBtn.setEnabled(Option(ev.getValue).isDefined)
        })

        acceptBtn.addClickListener(_ => {
          Option(choosePaywallCategoryCombo.getValue).map { pw =>
            val replyCategory = pw.category
            appContext.displayMsgCache.invalidate(sessId, detailedMsg.id)
            val replyContact = NewContactMessage(userId.value, detailedMsg.author, replyCategory, Some(detailedMsg.id))
            sendNewContactMessage(sessId.value, replyContact).await.genericHandle().map(_ => navigateHome())
          }
        })

        choseCategoryLayout.add(choosePaywallCategoryCombo, acceptBtn, ignoreBtn)
        decorateAvatar(avatarImage)
      }

      add(new H3("New contact request"), avatarImage, msgAuthorName, info, choseCategoryLayout)

  }

  private def decorateAvatar(avatarImage: Image): Image = {
    avatarImage.addClassName("avatar-image")
    avatarImage.getStyle.set("background-image", "url()")
    avatarImage
  }
}
