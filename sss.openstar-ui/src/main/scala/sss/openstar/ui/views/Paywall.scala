package sss.openstar.ui.views

import com.vaadin.flow.component.button.Button
import com.vaadin.flow.component.html.Label
import com.vaadin.flow.component.icon.VaadinIcon
import com.vaadin.flow.component.orderedlayout.{FlexComponent, FlexLayout, HorizontalLayout, VerticalLayout}
import com.vaadin.flow.component.textfield.TextField
import com.vaadin.flow.data.value.ValueChangeMode
import com.vaadin.flow.router.{PageTitle, Route}
import sss.openstar.Bridger._
import sss.openstar.ui.AppContext.bridge.{deletePaywallCategory, listPaywallCategories, maxCallWaitDuration, setPaywallCategory}
import sss.openstar.ui.rpc.ImplicitOps._
import sss.openstar.ui.rpc._
import sss.openstar.ui.views.support.{CurrentUISupport, PushSupport, RequireLoginSupportImpl, SessionIdSupport}
import sss.openstar.ui.{AppContext, MainLayout}
import sss.openstar.ui.views.support.NotificationSupport._

import scala.util.{Success, Try}

@Route(value = "paywall", layout = classOf[MainLayout])
@PageTitle("Paywall")
class Paywall extends ViewFrame
  with SessionIdSupport
  with PushSupport
  with CurrentUISupport
  with RequireLoginSupportImpl {

  def makeSaveButton(): Button = {
    val btn = new Button("Add")
    //btn.setIcon(VaadinIcon.CHECK.create())
    btn.setEnabled(false)
    btn.setVisible(false)
    btn
  }

  def makeDeleteButton(): Button = {
    val btn = new Button()
    btn.setIcon(VaadinIcon.TRASH.create())
    btn
  }

  def makeCategory(category: String): TextField = {
    val tf = new TextField("Category name")
    tf.setValue(category)
    tf.setReadOnly(true)
    tf
  }

  def makeAmount(amount: Long): TextField = {
    val tf = new TextField("Amount to charge")
    tf.setWidth("140px")
    tf.setValueChangeMode(ValueChangeMode.EAGER)
    tf.setValue(amount.toString)
    tf.setPrefixComponent(VaadinIcon.COIN_PILES.create())
    tf
  }

  def drawPaywallCategories(all: Seq[PaywallCategory]) = {

    all.map { pw =>
      val paywallCategory = new FlexLayout()
      paywallCategory.addClassName("paywall-category")
      paywallCategory.setAlignItems(FlexComponent.Alignment.BASELINE)
      val catComp = makeCategory(pw.category)
      val amComp = makeAmount(pw.amount)


      val saveBtn = makeSaveButton()
      saveBtn.addClickListener(ev => {
        Try(amComp.getValue.toLong) match {
          case Success(amount) if amount >= 0 =>
            setPaywallCategory(sessId.value, PaywallCategory(pw.category, amount)).await.genericHandle().foreach { _ =>
              push(setViewContent(createLayout()))
            }
          case _ =>
            push(showWarn(s"Amount must be number >= 0 (not ${amComp.getValue})"))
        }

      })

      amComp.addValueChangeListener(ev => {
        if (ev.getOldValue != ev.getValue && !saveBtn.isEnabled) {
          saveBtn.setEnabled(true)
          saveBtn.setVisible(true)
        }
      })


      val deleteBtn = makeDeleteButton()

      if (pw.category != "unknown") {
        deleteBtn.addClickListener(_ => {
          deletePaywallCategory(sessId.value, pw.category).await.genericHandle().foreach { _ =>
            push(setViewContent(createLayout()))
          }
        })
      } else {
        deleteBtn.setEnabled(false)
      }

      paywallCategory.add(catComp, amComp, saveBtn, deleteBtn)

      paywallCategory
    }
  }

  private def createLayout(): VerticalLayout = {
    val baseLayout = new VerticalLayout()

    val vertical = new VerticalLayout()
    vertical.setPadding(false)
    vertical.setSpacing(false)

    val info = new Label("This screen is used to configure charging categories. Each contact of your's will belong to single category.")
    baseLayout.add(info)

    val existingLabel = new Label("Existing categories")
    existingLabel.getStyle().set("margin-top", "24px")
    existingLabel.getStyle().set("font-weight", "600")
    vertical.add(existingLabel)

    val r = listPaywallCategories(sessId.value).genericHandle()
    val currCats = r.getOrElse(Seq())
    vertical.add(drawPaywallCategories(currCats): _*)

    baseLayout.add(vertical)

    val addLayout = new FlexLayout()
    addLayout.addClassName("paywall-category")
    addLayout.setAlignItems(FlexComponent.Alignment.BASELINE)
    val addCatComp = makeCategory("")
    addCatComp.setReadOnly(false)

    //addCatComp.addValueChangeListener()

    val addAmoutComp = makeAmount(0)
    val addSaveBtn = makeSaveButton()
    addSaveBtn.setEnabled(true)
    addSaveBtn.setVisible(true)

    addSaveBtn.addClickListener(_ => {
      val newCategory = addCatComp.getValue
      if (newCategory.nonEmpty && !currCats.exists(_.category == newCategory)) {
        Try(addAmoutComp.getValue.toLong) match {
          case Success(amount) if amount >= 0 =>
            setPaywallCategory(sessId.value, PaywallCategory(newCategory, amount)).await.genericHandle().foreach { _ =>
              push(setViewContent(createLayout()))
            }
          case _ =>
            showWarn(s"Amount must be number >= 0 (not ${addAmoutComp.getValue})")
        }
      } else {
        showWarn(s"Enter a unique category!")
      }
    })


    addLayout.add(addCatComp, addAmoutComp, addSaveBtn)

    val addLabel = new Label("Create new category")
    addLabel.getStyle().set("margin-top", "24px")
    addLabel.getStyle().set("font-weight", "600")

    vertical.add(addLabel, addLayout)
    baseLayout
  }

  setViewContent(createLayout())


}

