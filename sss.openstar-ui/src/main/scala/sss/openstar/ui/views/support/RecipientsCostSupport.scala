package sss.openstar.ui.views.support

import sss.openstar.ui.BaseHelper
import sss.openstar.Bridger.ProblemOps

trait RecipientsCostSupport {

  self: AppContextSupport
    with SessionIdSupport =>

  val defaultAmountPerRecipient = 0
  //Read and write of a volatile Long IS considered atomic.
  @volatile
  private var amountPerRecipient: Long = defaultAmountPerRecipient

  @volatile
  private var recipientsCostMap: Map[String, Long] = Map.empty


  def hasRecipient(recipient: String): Boolean = synchronized {
    recipientsCostMap.contains(recipient)
  }

  final def costToContact(recipient: String): Option[Long] =
    appContext.bridge.costToContact(sessId.value, recipient).genericHandle()

  final def addRecipient(recipient: String, amount: Long): Unit = {
    synchronized {
      recipientsCostMap += (recipient -> amount)
    }
    fire()
  }

  final def removeRecipients(): Unit = {
    recipients foreach removeRecipient
  }

  final def removeRecipient(recipient: String): Unit = {
    synchronized {
      recipientsCostMap -= recipient
    }
    fire()
  }

  def getAmountPerRecipient(): Long = synchronized {
    amountPerRecipient
  }

  def setAmountPerRecipient(amount: Long): Unit = {
    synchronized {
      amountPerRecipient = amount
    }
    fire()
  }

  final def recipients: Set[String] = synchronized {
    recipientsCostMap.keySet
  }

  def calculateTotalCost(): Long = synchronized {
    recipientsCostMap.values.sum + (recipientsCostMap.keySet.size * amountPerRecipient)
  }

  def isBalanceExceeded(): Option[Boolean] = {
    BaseHelper.getCachedBalance().map { bal =>
      calculateTotalCost() > bal
    }
  }

  private def fire(): Unit = {
    val nowCost = calculateTotalCost()
    onTotalCostChange(nowCost)
    BaseHelper.getCachedBalance() foreach { balance =>
      if(nowCost > balance) onBalanceExceeded(nowCost, balance)
    }
  }

  def onBalanceExceeded(totalCost: Long, balance: Long): Unit

  def onTotalCostChange(newAmount: Long): Unit

}
