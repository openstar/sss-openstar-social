package sss.openstar.ui.views

import com.vaadin.flow.component.HasComponents
import com.vaadin.flow.component.button.{Button, ButtonVariant}
import com.vaadin.flow.component.combobox.ComboBox
import com.vaadin.flow.component.dialog.Dialog
import com.vaadin.flow.component.html.Label
import com.vaadin.flow.component.orderedlayout.{HorizontalLayout, VerticalLayout}
import sss.ancillary.Logging
import sss.openstar.ui.AppContext.bridge.{listPaywallCategories, maxCallWaitDuration, sendNewContactMessage}
import sss.openstar.ui.rpc.{NewContactMessage, PaywallCategory}
import sss.openstar.ui.{AppContext, BaseHelper}
import sss.openstar.ui.rpc.ImplicitOps._
import sss.openstar.ui.views.render.RenderMessageFactory.OnDone
import sss.openstar.ui.views.support.NotificationSupport.showWarn
import sss.openstar.ui.views.support.{AppContextSupport, ChooseRecipientsSupport, RecipientsCostSupport, SessionIdSupport, UserIdSupport}
import sss.openstar.Bridger.ProblemOps

import scala.jdk.CollectionConverters._

class VerticalLayoutInviteNewContact(val onDone: OnDone, to: String = "")(implicit val appContext: AppContext)
  extends VerticalLayout
    with InviteNewContact
    with SessionIdSupport
    with Logging
    with AppContextSupport
    with RecipientsCostSupport
    with ChooseRecipientsSupport
    with UserIdSupport {

      addClassName("centered-layout")
      addClassName("invite-new-contact")

      override protected def isIgnored(identity: String): Boolean = {
        BaseHelper.getContacts(sessId).exists(_.exists(_.name == identity)) || userId.value == identity
      }

}

class DialogInviteNewContact(val onDone: OnDone, to: String = "")(implicit val appContext: AppContext)
                             extends Dialog
                             with InviteNewContact

                               with SessionIdSupport
                               with Logging
                               with AppContextSupport
                               with RecipientsCostSupport
                               with ChooseRecipientsSupport
                               with UserIdSupport {



  override protected def isIgnored(identity: String): Boolean = {
    BaseHelper.getContacts(sessId).exists(_.exists(_.name == identity)) || userId.value == identity
  }
}

trait InviteNewContact {

  self : HasComponents

    with SessionIdSupport
    with Logging
    with AppContextSupport
    with RecipientsCostSupport
    with ChooseRecipientsSupport
    with UserIdSupport =>

  val onDone: OnDone


  override def onBalanceExceeded(totalCost: Long, balance: Long): Unit = {
    sendButton.setEnabled(false)
    showWarn(s"Total cost (${totalCost}) exceeds available balance ${balance}, get more starz")
  }

  override protected val recipientsCombo: ComboBox[String] = createRecipientCombo()
  recipientsCombo.setLabel("Recipient")
  recipientsCombo.setPlaceholder("Contact to add")
  recipientsCombo.setWidthFull()

  val paywallLabel = new Label("Select one or more recipients you would like to add to your contacts. Paywall category defines the cost the contact has to pay you for starting a conversation.")
  val paywallCombo = new ComboBox[PaywallCategory]
  paywallCombo.setLabel("Paywall category for recipients")
  paywallCombo.setPlaceholder("Defines how much contact is being charged for contacting you")
  paywallCombo.setWidthFull()
  val inviteesLayout = new HorizontalLayout()
  addChosenRecipientsToLayout(inviteesLayout)
  //val paywallsLayout = new VerticalLayout(paywallLabel, recipientsCombo, inviteesLayout, paywallCombo, sendButton)

  def toDisplayString(wall: PaywallCategory): String =
    s"${wall.category} (Cost:${wall.amount})"


  def drawPaywalls(): Unit = {
    listPaywallCategories(sessId.value).genericHandle().foreach { paywalls =>
      paywallCombo.setAllowCustomValue(false)
      paywallCombo.setItemLabelGenerator(toDisplayString)
      paywallCombo.setItems(paywalls.asJavaCollection)

    }
  }

  drawPaywalls()

  override def onTotalCostChange(newCost: Long): Unit = {
    sendButton.setEnabled(true)
    sendButton.setText(s"Send ($newCost)")
  }

  val sendButton = new Button("Send")
  sendButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY)
  sendButton.setEnabled(false)

  recipientsCombo.addValueChangeListener(_ => {
    sendButton.setEnabled(recipients.nonEmpty && !(isBalanceExceeded().getOrElse(false)))
  })



  sendButton.addClickListener(_ => {
    Option(paywallCombo.getValue) foreach { paywallCategory =>
      recipients foreach { recipient =>
        sendNewContactMessage(
          sessId.value,
          NewContactMessage(
            userId.value,
            recipient,
            paywallCategory.category,
            None
          )
        ).await.genericHandle().map(_ => onDone())
      }
    }
  })

  add(paywallLabel, recipientsCombo, inviteesLayout, paywallCombo, sendButton)


}
