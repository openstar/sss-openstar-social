package sss.openstar.ui.views.support

trait RequireLoginSupportImpl extends RequireLoginSupport {
  override protected val loginRoot: String = "login"
}
