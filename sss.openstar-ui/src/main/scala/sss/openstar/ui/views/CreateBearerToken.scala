package sss.openstar.ui.views

import com.vaadin.flow.component.button.Button
import com.vaadin.flow.component.checkbox.Checkbox
import com.vaadin.flow.component.icon.VaadinIcon
import com.vaadin.flow.component.notification.Notification
import com.vaadin.flow.component.orderedlayout.{ FlexComponent, HorizontalLayout, VerticalLayout }
import com.vaadin.flow.component.textfield.TextField
import com.vaadin.flow.component.{ Component, HasSize, ItemLabelGenerator }
import com.vaadin.flow.router.{ BeforeEnterEvent, BeforeEnterObserver, PageTitle, Route }
import sss.openstar.Bridger._
import sss.openstar.ui.{ AppContext, MainLayout }
import sss.openstar.ui.rpc._
import sss.openstar.ui.views.Providers.StoreHolder
import sss.openstar.ui.views.support.{
  CurrentUISupport,
  NotificationSupport,
  PushSupport,
  RequireLoginSupportImpl,
  SessionIdSupport
}

import scala.math.BigDecimal

@Route(value = "apitokens", layout = classOf[MainLayout])
@PageTitle("API Tokens")
class CreateBearerToken()
    extends ViewFrame
    with SessionIdSupport
    with CurrentUISupport
    with PushSupport
    with RequireLoginSupportImpl {

  addClassName("centered-layout")

  private val providersLayout = new VerticalLayout()
  private val secretField = new TextField("Use this secret in API calls where an 'api secret' is required")
  private val secretLayout = new HorizontalLayout(secretField)
  secretField.setWidthFull()

  secretLayout.setAlignItems(FlexComponent.Alignment.BASELINE)
  secretLayout.setWidth("100%")
  secretLayout.setVisible(false)

  private def getHashedToken: Option[String] =
    AppContext.bridge
      .getHashedIdentityBearerAttribute(sessId())
      .genericHandle()
      .flatten

  private def createNewToken(): Unit =
    AppContext.bridge.createIdentityBearerAttribute(sessId()).genericHandle().foreach { secret =>
      secretField.setValue(secret)
      secretLayout.setVisible(true)
      NotificationSupport.showWarn("Make a copy of this secret right now! It will not be shown again.", 10000)
    }

  def drawLayout: Seq[HorizontalLayout] = {

    val existingTokenField = new TextField()

    existingTokenField.setEnabled(false)
    existingTokenField.setLabel("Hash of existing token (do not copy, this is not a secret)")
    existingTokenField.setWidthFull()
    existingTokenField.setValue("No Token")
    val check = new Checkbox("I want to replace existing API token")
    check.setEnabled(false)

    getHashedToken.foreach { token =>
      existingTokenField.setValue(token)
      check.setEnabled(true)
    }

    val exitLayout = new HorizontalLayout(existingTokenField)
    exitLayout.setAlignItems(FlexComponent.Alignment.BASELINE)
    exitLayout.setWidth("100%")

    val createNewTokenBtn = new Button("Create New Token")

    createNewTokenBtn.setIcon(VaadinIcon.PAPERPLANE_O.create())

    createNewTokenBtn.addClickListener { e =>
      if (!check.isEnabled || check.getValue) {
        createNewToken()

        providersLayout.removeAll()
        providersLayout.add(drawLayout: _*)
      } else {
        NotificationSupport.show("You must replace any existing API token")
      }

    }

    val hl = new HorizontalLayout(check, createNewTokenBtn)
    hl.setAlignItems(FlexComponent.Alignment.BASELINE)
    hl.setWidth("100%")


    Seq(exitLayout, hl, secretLayout)
  }

  providersLayout.add(drawLayout: _*)

  setViewContent(providersLayout)

}
