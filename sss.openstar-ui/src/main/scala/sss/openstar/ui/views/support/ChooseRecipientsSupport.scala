package sss.openstar.ui.views.support

import com.vaadin.flow.component.combobox.ComboBox
import com.vaadin.flow.component.html.Div
import com.vaadin.flow.component.icon.{Icon, VaadinIcon}
import com.vaadin.flow.component.orderedlayout.FlexComponent
import com.vaadin.flow.shared.Registration
import sss.openstar.ui.util.CoinUtil
import sss.openstar.ui.views.support.ChooseRecipientsSupport.RecipientControl

object ChooseRecipientsSupport {
  case class RecipientControl(div: Div, closeIcon: Icon)
}

trait ChooseRecipientsSupport {

  self: AppContextSupport
    with SessionIdSupport
    with RecipientsCostSupport =>

  protected val recipientsCombo: ComboBox[String]

  protected def makeRecipientControl(id: String, amount: Option[Long]): RecipientControl = {

    val amountTxt = amount.map(_.toString).getOrElse("?")
    val coinComp = CoinUtil.createAmountComponent(amountTxt)

    coinComp.userLabel.setText(id.replaceAll("_", " "))

    val close = VaadinIcon.CLOSE.create()
    coinComp.div.addClassName("token")
    close.setSize("10px")
    close.getStyle.set("padding-left", "4px")
    coinComp.div.add(close)
    RecipientControl(coinComp.div, close)
  }


  protected def isIgnored(identity: String): Boolean = false

  protected def addCloseableNewRecipient(id: String, targetLayout: FlexComponent[_]): Unit = {

    if (!hasRecipient(id) && !isIgnored(id)) {

      val totalOpt = costToContact(id)
      val RecipientControl(div, close) = makeRecipientControl(id, totalOpt)

      totalOpt.foreach{ total =>
        addRecipient(id, total)
      }

      close.addClickListener(e => {
        removeRecipient(id)
        targetLayout.remove(div)
      })

      targetLayout.add(div)
    }
  }


  protected def addChosenRecipientsToLayout(targetLayout: FlexComponent[_]): Registration = {
    recipientsCombo.addValueChangeListener(e => {
      Option(e.getValue) map { newRecipient =>
          addCloseableNewRecipient(newRecipient, targetLayout)
      }
    })
  }

  protected def createRecipientCombo(): ComboBox[String] = {
    val combo = new ComboBox[String]()
    combo.setAllowCustomValue(false)
    combo.setDataProvider(appContext.identitiesDataProvider.create)

    combo
  }
}
