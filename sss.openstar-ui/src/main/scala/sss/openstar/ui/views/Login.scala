package sss.openstar.ui.views

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.{Failure, Success}
import com.vaadin.flow.component._
import com.vaadin.flow.component.button.Button
import com.vaadin.flow.component.orderedlayout.FlexComponent.JustifyContentMode
import com.vaadin.flow.router.{BeforeEnterEvent, BeforeEnterObserver, PageTitle, Route}
import sss.ancillary.Logging
import sss.openstar.Bridger._
import sss.openstar.ui.AppContext.bridge
import sss.openstar.ui.AppContext.as
import sss.openstar.ui.components.FlexBoxLayout
import sss.openstar.ui.html5push.{Subscription, SubscriptionJs}
import sss.openstar.ui.rpc.{SessionId, SubscriptionMessage}
import sss.openstar.ui.util.{IdentityUtil, SessionTracker}
import sss.openstar.ui.views.support.{ClientSideCredentialsSupport, NotificationSupport, PushSupport}
import sss.openstar.ui.{LoginSnippet, MainLayout}
import sss.openstar.ui.rpc._

import concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{Await, Future}
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.{Failure, Success}


@Route(value = "", layout = classOf[MainLayout])
@PageTitle("Login")
class Login extends ViewFrame
  with Logging
  with BeforeEnterObserver
  with ClientSideCredentialsSupport {

  private val login: LoginSnippet = new LoginSnippet()

  private var btnShortcutRegistration: Option[ShortcutRegistration] = None

  setId("login")


  private def createContent = {

    val content = new FlexBoxLayout(login)
    content.setJustifyContentMode(JustifyContentMode.CENTER)
    content.setHeightFull()
    content
  }

  private def attemptNotificationRegistration(ui: UI): Future[Subscription] = {
    new SubscriptionJs(ui.getPage).getSubscription(ui)
  }

  override def beforeEnter(event: BeforeEnterEvent): Unit = {

    if (SessionTracker.isLoggedIn(event.getUI)) {
      event.forwardTo(classOf[InBox])
    } else {
      retrieveCredentialsFromBrowser(event.getUI, bridge).onComplete {
        case Success(Right((user, pass))) =>
          loginUserAndGotoInBox(user, pass)(event.getUI).recover {
            case x =>
              log.debug("Login failed", x)
              problem(x)
          }.map { result =>
            PushSupport.push {
              result.genericHandle()
              setViewContent(createContent)
            }(event.getUI)
          }

        case x =>
          log.debug("No credentials retrieved, no auto login")
          PushSupport.push(setViewContent(createContent))(event.getUI)

      }

    }
  }

  override protected def onAttach(attachEvent: AttachEvent): Unit = {
    super.onAttach(attachEvent)

    MainLayout.get.getNaviDrawer.setVisible(false)
    MainLayout.get.getAppBar.setVisible(false)

    attachEnterToUnlock()

    login.username.setPattern("^[a-zA-Z][a-zA-Z\\s]*$")
    login.username.setPreventInvalidInput(true)

    login.registerBtn.addClickListener(e => {
      val user = IdentityUtil.clean(login.username.getValue)
      val pass = login.phrase.getValue
      val pass2 = login.rephrase.getValue
      if (login.rephrase.isEnabled) {

        createUserAndGotoInBox(user, pass, pass2)(e.getSource.getUI.get)


      } else {
        login.username.setPlaceholder("your new identity")
        login.rephrase.setEnabled(true)
        attachEnterToRegister()
      }
    })


    def attachEnterToUnlock(): Unit = {
      btnShortcutRegistration.foreach { reg =>
        reg.remove()
      }
      btnShortcutRegistration = Some(login.unlockBtn.addClickShortcut(Key.ENTER))
    }

    def attachEnterToRegister(): Unit = {
      btnShortcutRegistration.foreach { reg =>
        reg.remove()
      }
      btnShortcutRegistration = Some(login.registerBtn.addClickShortcut(Key.ENTER))
    }


    login.unlockBtn.addClickListener((e: ClickEvent[Button]) => {
      val user = IdentityUtil.clean(login.username.getValue)
      val pass = login.phrase.getValue
      login.username.setRequired(true)
      login.phrase.setRequired(true)
      login.rephrase.setEnabled(false)

      if (e.getSource.getUI.isPresent) {
        implicit val ui = e.getSource.getUI.get
        loginUserAndGotoInBox(user, pass).onComplete {
          case Success(r) => PushSupport.push(r.genericHandle())
          case Failure(e) =>
            log.warn("No can log in", e)
            PushSupport.push(NotificationSupport.show("Can't log in."))

        }
      } else {
        log.warn("Couldn't get UI from event, can't continue")
      }


    })
  }

  private def loginUserAndGotoInBox(user: String, pass: String)(implicit ui: UI) : Future[Result[Unit]]= {
    val sessId = SessionTracker.sessId(ui)
    attemptNotificationRegistration(ui) map Option.apply recover {
      case exception =>
        log.warn("Failed to get sub", exception)
        None
    } map { subOpt =>

      val rpcSubOpt = subOpt.map(sub => SubscriptionMessage(sub.endpoint, sub.key, sub.auth))
        bridge.login(sessId.value, user, pass, rpcSubOpt).map(_ => {

          if (login.shownotify.getValue) {
            // async call
            storeCredentialsInBrowser(user, pass)(ui, bridge)
              .recover {
                case e => log.warn("Failed to store credentials in clients browser", e)
              }
          }
          PushSupport.push(initSessionGoToInBox(user, sessId))
        })
    }
  }

  private def createUserAndGotoInBox(user: String, pass: String, pass2: String)(implicit ui: UI) = {
    val sessId = SessionTracker.sessId(ui)
    attemptNotificationRegistration(ui).map { sub =>
      val rpcSub = SubscriptionMessage(sub.endpoint, sub.key, sub.auth)
      Some(rpcSub)
    }.recover {
      case x => None
    }.map { rpcSubOpt =>
      PushSupport.push {
        val createResult = bridge.createUser(sessId.value, user, pass, pass2, rpcSubOpt).genericHandle()
        createResult.foreach(_ => {
          if (login.shownotify.getValue) {
            // async call
            storeCredentialsInBrowser(user, pass)(ui, bridge)
              .recover {
                case e => log.warn("Failed to store credentials in clients browser", e)
              }
          }

          initSessionGoToInBox(user, sessId)

        })
      }
    }
  }


  private def initSessionGoToInBox(user: String, sessId: SessionId) = {
    MainLayout.get.getNaviDrawer.setVisible(true)
    MainLayout.get.getAppBar.setVisible(true)
    SessionTracker.login(user)


    MainLayout.get.getAppBar.userLabel.setText(user.replaceAll("_", " "))

    bridge.consumeEvents(sessId.value)

    MainLayout.get.getAppBar.initAvatar()
    MainLayout.get.getAppBar.updateBalance()
    MainLayout.get.getAppBar.registerForBalanceUpdates();

    UI.getCurrent.navigate(classOf[InBox])
  }
}


