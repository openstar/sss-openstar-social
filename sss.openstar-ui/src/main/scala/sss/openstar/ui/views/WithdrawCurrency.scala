package sss.openstar.ui.views

import com.vaadin.flow.component.button.Button
import com.vaadin.flow.component.combobox.ComboBox
import com.vaadin.flow.component.icon.VaadinIcon
import com.vaadin.flow.component.notification.Notification
import com.vaadin.flow.component.orderedlayout.{FlexComponent, HorizontalLayout, VerticalLayout}
import com.vaadin.flow.component.textfield.TextField
import com.vaadin.flow.component.{Component, HasSize, ItemLabelGenerator}
import com.vaadin.flow.data.value.ValueChangeMode
import com.vaadin.flow.router.{PageTitle, Route}
import sss.openstar.Bridger._
import sss.openstar.ui.AppContext.bridge.{addProvider, listProviders, maxCallWaitDuration, removeProvider}
import sss.openstar.ui.rpc.ImplicitOps._
import sss.openstar.ui.rpc._
import sss.openstar.ui.util.RegisteredProvidersDataProvider
import sss.openstar.ui.views.Providers.{RegisteredProviderLabelGenerator, StoreHolder}
import sss.openstar.ui.views.support.{CurrentUISupport, PushSupport, RequireLoginSupportImpl, SessionIdSupport}
import sss.openstar.ui.{AppContext, MainLayout}

import scala.math.{BigDecimal, floor}
import scala.util.Try


object WithdrawCurrency {
  case class StoreHolder(l: HorizontalLayout, b: Button, t: Component)

  object RegisteredProviderLabelGenerator extends ItemLabelGenerator[RegisteredProvider] {
    override def apply(t: RegisteredProvider): String = s"${t.name} (charges ${t.amount})"
  }
}

@Route(value = "withdraw", layout = classOf[MainLayout])
@PageTitle("Withdraw Currency")
class WithdrawCurrency() extends ViewFrame
  with SessionIdSupport
  with CurrentUISupport
  with PushSupport
  with RequireLoginSupportImpl {

  addClassName("centered-layout")

  def makeStoreHolder(middleComponent: Component with HasSize): StoreHolder = {
    val hl = new HorizontalLayout()
    hl.setAlignItems(FlexComponent.Alignment.BASELINE)
    hl.setWidth("100%")
    val btn = new Button()
    btn.setIcon(VaadinIcon.TRASH.create())
    //btn.getS.addStyleNames("inline-icon")
    middleComponent.setWidth("100%")
    hl.add(middleComponent, btn)
    StoreHolder(hl, btn, middleComponent)
  }

  private val providersLayout = new VerticalLayout()

  val OneMillion: Long = 1000000

  private def longToNearestMillion(in: Long): Long = {
    BigDecimal(in / OneMillion).toLongExact
  }

  private def getMaxWithdrawal(): Long = {
    val inLovelace = AppContext
      .bridge
      .balance(sessId().value)
      .genericHandle(Some("Failed to get balance."))
      .getOrElse(0L)

    longToNearestMillion(inLovelace) * OneMillion

  }

  private def sendAdaToAddress(amount: Long, outAddr: String):Unit = {

    AppContext.bridge.currencyWithdrawal(sessId, amount, outAddr).genericHandle()

  }

  def drawAmountLayout: Seq[HorizontalLayout] = {


    val exitAddr = new TextField()

    exitAddr.setLabel("Exit address")
    exitAddr.setWidthFull()

    val exitLayout = new HorizontalLayout(exitAddr)
    exitLayout.setAlignItems(FlexComponent.Alignment.BASELINE)
    exitLayout.setWidth("100%")


    val amountToWithdraw = new TextField()
    amountToWithdraw.setLabel("Amount to withdraw")
    amountToWithdraw.setWidthFull()

    val maxWithdrawal = getMaxWithdrawal()
    amountToWithdraw.setValue(maxWithdrawal.toString)

    val sendTxBtn = new Button()

    sendTxBtn.setIcon(VaadinIcon.PAPERPLANE_O.create())

    sendTxBtn.addClickListener(e => {
      val value = amountToWithdraw.getValue
      val addrValue = if(exitAddr.getValue.startsWith("addr")) Some(exitAddr.getValue) else None
      (addrValue, value.toLongOption) match {
        case (None, _) =>
          Notification.show(s"Couldn't parse valid address")
        case (_, None) =>
          Notification.show(s"Couldn't parse $value")
        case (_, Some(withdrawalAmount)) if(withdrawalAmount <= 0) =>
          Notification.show(s"$value must be greater than '0'")
        case (_, Some(withdrawalAmount)) if withdrawalAmount > maxWithdrawal =>
          Notification.show(s"Max allowed is $maxWithdrawal")
        case (Some(addr), Some(goodAmount)) =>
          sendAdaToAddress(goodAmount, addr)
      }

    })

    val hl = new HorizontalLayout(amountToWithdraw, sendTxBtn)
    hl.setAlignItems(FlexComponent.Alignment.BASELINE)
    hl.setWidth("100%")

    Seq(exitLayout, hl)
  }

  providersLayout.add(drawAmountLayout: _*)


  setViewContent(providersLayout)

}

