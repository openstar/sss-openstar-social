package sss.openstar.ui.views.render

import com.vaadin.flow.component.Component
import sss.openstar.ui.AppContext
import sss.openstar.ui.rpc.{DetailedNewContactMessage, MessagePayloadType, SessionId}
import sss.openstar.ui.rpc.MessagePayloadType.MessagePayloadType
import sss.openstar.ui.views.{VerticalLayoutInviteNewContact, ViewInviteNewContact}
import sss.openstar.ui.views.render.RenderMessageFactory.OnDone
import sss.openstar.ui.views.render.RenderMessageFactory.ViewType.ViewType

class ContactInviteRenderer extends RenderMessageFactory[DetailedNewContactMessage] {

  val name: String = "New Contact"
  val description: String = "Send New Contact Invite "
  val payloadType: MessagePayloadType = MessagePayloadType.NewContactType

  def createNewWriteComponent(
                               onDone: OnDone
                             )(implicit appContext: AppContext, sessId: SessionId): Component = {

    new VerticalLayoutInviteNewContact(onDone)
  }

  def createRenderer(
              viewType: ViewType
            )(implicit appContext: AppContext, sessId: SessionId): MessageRenderer[DetailedNewContactMessage] =

    SimpleMessageRenderer {
      case contactMsg: DetailedNewContactMessage => Some(new ViewInviteNewContact(contactMsg))
      case m => NullMessageRenderer().render(m)
    }


}
