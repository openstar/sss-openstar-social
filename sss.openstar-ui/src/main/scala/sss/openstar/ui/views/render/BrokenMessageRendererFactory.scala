package sss.openstar.ui.views.render

import com.vaadin.flow.component.Component
import com.vaadin.flow.component.orderedlayout.HorizontalLayout
import sss.openstar.ui.AppContext
import sss.openstar.ui.rpc.{DetailedDisplayMessage, MessagePayloadType, SessionId}
import sss.openstar.ui.rpc.MessagePayloadType.MessagePayloadType
import sss.openstar.ui.views.render.RenderMessageFactory.OnDone
import sss.openstar.ui.views.render.RenderMessageFactory.ViewType.ViewType

class BrokenMessageRendererFactory extends RenderMessageFactory[DetailedDisplayMessage] {

  val name: String = "Broken or missing"
  val description: String = "Badly formed or unknown message"
  val payloadType: MessagePayloadType = MessagePayloadType.BrokenMessageType

  def createNewWriteComponent(
                               onDone: OnDone
                             )(implicit appContext: AppContext, sessId: SessionId): Component = new HorizontalLayout()

  def createRenderer(
              viewType: ViewType
            )(implicit appContext: AppContext, sessId: SessionId): MessageRenderer[DetailedDisplayMessage] = NullMessageRenderer()

}
