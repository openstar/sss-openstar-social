package sss.openstar.ui.views.support

import sss.openstar.ui.AppContext

trait AppContextSupport {
  implicit val appContext: AppContext
}
