package sss.openstar.ui.views.support


import java.util.Optional

import com.vaadin.flow.component.button.Button
import com.vaadin.flow.component.html.{Anchor, Div, Label}
import com.vaadin.flow.component.icon.{Icon, VaadinIcon}
import com.vaadin.flow.component.{Component, HasComponents}
import com.vaadin.flow.server.{InputStreamFactory, StreamResource}
import sss.openstar.ui.components.ExpandableImage
import sss.openstar.ui.util.FormatUtil

trait AttachmentRenderSupport {

  def containsFileName(fileName: String, fromLayout: Component with HasComponents): Boolean = {
    fromLayout
      .getChildren
      .filter(_.getId == Optional.of(makeId(fileName)))
      .count() > 0
  }

  def removeAttachmentComponent(fileName: String, fromLayout: Component with HasComponents): Unit = {
    fromLayout
      .getChildren
      .filter(_.getId == Optional.of(makeId(fileName)))
      .forEach(found => fromLayout.remove(found))
  }

  def removeAttachmentComponents(fromLayout: Component with HasComponents): Unit = {
    fromLayout
      .getChildren
      .filter(_.getId.orElse("").startsWith(prefix))
      .forEach(found => fromLayout.remove(found))
  }

  private val prefix = "ATTACHMENT"

  private def makeId(fileName: String): String = {
    s"$prefix$fileName"
  }

  def createAttachmentComponent(
                                 fileName: String,
                                 fileSize: Long,
                                 aMIMEType: String,
                                 streamFactory: InputStreamFactory):Component = {

    val fileWrapper = new Div()
    fileWrapper.addClassNames("file")

    val fileDetails = new Div()
    fileDetails.addClassName("file__details")
    val fileTitle = new Label(fileName)
    fileTitle.addClassName("file__title")
    val fileSizeString = new Label(FormatUtil.humanReadableByteSize(fileSize))
    fileDetails.add(fileTitle, fileSizeString)

    var fileActions = new Div()
    fileActions.addClassName("file__actions")
    val fileDownload = new Anchor(new StreamResource(fileName, streamFactory), "")
    fileDownload.add(new Button(new Icon(VaadinIcon.DOWNLOAD)))
    fileDownload.getElement.setAttribute("download", true)
    fileActions.add(fileDownload)

    if(aMIMEType.startsWith("image/")) {
      fileWrapper.addClassName("file__image")

      val image = new ExpandableImage()
      image.setSrc(new StreamResource(fileName, streamFactory))
      image.setWidth("-1")
      image.setId(fileName)

      val imageWrapper = new Div(image)
      imageWrapper.addClassName("file__image-wrapper")
      fileWrapper.add(imageWrapper, fileDetails, fileActions)
    } else {

      fileWrapper.addClassName("file__document")

      val icon = new Div(new Icon(VaadinIcon.FILE_TEXT_O))
      icon.addClassName("file__document-icon")

      fileWrapper.add(icon, fileDetails, fileActions)
    }

    fileWrapper.setId(makeId(fileName))
    fileWrapper
  }

}
