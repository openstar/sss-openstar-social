package sss.openstar.ui.views

import scala.util.Try


import com.vaadin.flow.component.combobox.ComboBox
import com.vaadin.flow.component.orderedlayout.FlexComponent.Alignment
import com.vaadin.flow.component.orderedlayout.{HorizontalLayout, VerticalLayout}
import com.vaadin.flow.component.textfield.TextField
import com.vaadin.flow.data.value.ValueChangeMode
import sss.ancillary.Logging
import sss.openstar.ui.AppContext
import sss.openstar.ui.views.render.RenderMessageFactory.OnDone
import sss.openstar.ui.views.support.{NotificationSupport, _}


class WriteMessage(
                                  val onDone: OnDone
                                  )(implicit override val appContext: AppContext)
  extends VerticalLayout
    with Logging
    with WriteMessageSupport
    with AttachmentRenderSupport

    with UserIdSupport
    with AppContextSupport
    with SessionIdSupport
    with RecipientsCostSupport
    with ChooseRecipientsSupport {

  val parentId: Option[Long] = None

  val toCombo: ComboBox[String] = new ComboBox[String]()
  toCombo.setPlaceholder("To")

  private val timetoLive: TextField = new TextField()
  timetoLive.setPlaceholder("Reclaim after")
  timetoLive.setTitle("If not delivered to a recipient starz are eventually reclaimed by the sender, 1 block ~= 1 hour")

  timetoLive.setValueChangeMode(ValueChangeMode.EAGER)


  timetoLive.addValueChangeListener(e => {
    val textField = e.getSource
    val newValue = toIntGtZero(textField).getOrElse(defaultNumBlockToLive)
    textField.setValue(newValue.toString)
    setNumBlocksToLive(newValue)
  })

  private val tosLayout: HorizontalLayout = new HorizontalLayout(toCombo)
  tosLayout.setAlignItems(Alignment.CENTER)

  private val coinAmount: TextField = new TextField()
  coinAmount.setPlaceholder("Attach Coin Amount")

  private val coinLayout: HorizontalLayout = new HorizontalLayout(coinAmount, timetoLive)

  coinAmount.setValueChangeMode(ValueChangeMode.EAGER)

  coinAmount.addValueChangeListener(e => {
    val amountInEachMessage = toIntGtZero(coinAmount).getOrElse(defaultAmountPerRecipient)
    setAmountPerRecipient(amountInEachMessage)
    coinAmount.setValue(amountInEachMessage.toString)
  })

  val recipientsCombo: ComboBox[String] = toCombo
  recipientsCombo.setAllowCustomValue(false)
  recipientsCombo.setDataProvider(AppContext.identitiesDataProvider.create)

  addChosenRecipientsToLayout(tosLayout)


  addComponentAtIndex(0,tosLayout)
  addComponentAtIndex(1,coinLayout)



  private def toIntGtZero(field: TextField): Option[Int] = {
    if (field.isEmpty) {
      None
    } else Try {
      val r = field.getValue.toInt
      require(r > 0)
      r
    }.toOption
  }

  override def validate() : Option[String] = {
    val validation = super.validate()
    if(validation.isEmpty) {
      if (recipients.isEmpty || recipients == Set(userId.value)) {
        Some("Pick a recipient (that's not you)")
      } else None
    } else validation
  }


}



