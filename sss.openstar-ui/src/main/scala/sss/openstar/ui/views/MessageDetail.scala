package sss.openstar.ui.views

import scala.util.Try
import com.vaadin.flow.component._
import com.vaadin.flow.component.button.Button
import com.vaadin.flow.router._
import sss.ancillary.Logging
import sss.openstar.Bridger._
import sss.openstar.ui.AppContext._
import sss.openstar.ui.MainLayout
import sss.openstar.ui.components.navigation.bar.BaseAppBar
import sss.openstar.ui.rpc.SessionPushEventManager.{NewMessagePushEvent, PollEvent, PushEvent, RegistrationId}
import sss.openstar.ui.rpc.{DetailedDisplayMessage, DetailedEncryptedMessage, SessionPushEventManager}
import sss.openstar.ui.util.SessionTracker
import sss.openstar.ui.views.render.MessageRenderer
import sss.openstar.ui.views.render.RenderMessageFactory.ViewType
import sss.openstar.ui.views.support.{PushSupport, RequireLoginSupportImpl, SessionIdSupport, UserIdSupport}

@Route(value = "messagedetail", layout = classOf[MainLayout])
@PageTitle("")
class MessageDetail extends ViewFrame
  with HasUrlParameter[java.lang.Long]
  with SessionIdSupport
  with UserIdSupport
  with Logging
  with RequireLoginSupportImpl
  with BeforeLeaveObserver {

  private var mId: Long = -1;
  private var messageRendererOpt: Option[MessageRenderer[DetailedDisplayMessage]] = None

  private var dynamicLayout: Option[Component] = None

  private var registrationNewMsg: Option[RegistrationId] = None
  private var registrationMsgPoll: Option[RegistrationId] = None


  override protected def onAttach(attachEvent: AttachEvent): Unit = {
    super.onAttach(attachEvent)

    val appBar = initAppBar
    appBar.setTitle("")
    registerCallbacksAndRender(attachEvent.getUI)
  }

  override def beforeEnterPostLogin(beforeEnterEvent: BeforeEnterEvent): Unit = {
    registerCallbacksAndRender(beforeEnterEvent.getUI)
  }

  private def registerCallbacksAndRender(ui: UI): Unit = {


    if (mId != -1) {
      implicit val impUi: UI = ui

      def onMessage(ev: PushEvent): Unit = {
        ev match {
          case NewMessagePushEvent(msgId) if (msgId) == mId =>
            PushSupport.push {
              update(mId, ui)
            }
          case x =>
            log.debug("Unused event {}", x)
        }
      }

      render(this.mId, ui) map {
        case _: DetailedEncryptedMessage if(registrationNewMsg.isEmpty) =>
          registrationNewMsg =
            SessionTracker
              .eventManagerOpt()
              .map(
                _.register(SessionPushEventManager.PushEventType.NewMsgType, onMessage)
              )

          registrationMsgPoll =
            SessionTracker.eventManagerOpt().map(_.register(SessionPushEventManager.PushEventType.PollType, {
              case PollEvent =>
                PushSupport.push(update(mId, ui))
              case _ =>
            })
          )
        case _ =>
      }
    }

  }
  override def onDetach(detachEvent: DetachEvent): Unit = {
    deRegisterAll()
    super.onDetach(detachEvent)
  }

  private def deRegisterAll() = Try {
    registrationNewMsg.foreach { regId =>
      SessionTracker.eventManagerOpt().map(_.deRegister(regId))
    }
    registrationNewMsg = None

    registrationMsgPoll.foreach { regId =>
      SessionTracker.eventManagerOpt().map(_.deRegister(regId))
    }

    registrationMsgPoll = None
  } recover {
    case e => log.warn("Error attempting to dregister: {}", e)
  }

  override def beforeLeave(beforeLeaveEvent: BeforeLeaveEvent): Unit = {
    deRegisterAll()
  }

  private def initAppBar = {
    val appBar = MainLayout.get.getAppBar
    appBar.setNaviMode(BaseAppBar.NaviMode.CONTEXTUAL)
    appBar.getContextIcon.addClickListener((e: ClickEvent[Button]) => UI.getCurrent.navigate(classOf[InBox]))
    appBar
  }


  private def update(msgId: Long, ui: UI): Unit = {
    getMessage(msgId, ui).foreach(update(_, getContent, ui))
  }

  private def update(msg: DetailedDisplayMessage, content: HasComponents, ui: UI): Unit = {

    messageRendererOpt.foreach { messageRenderer =>
      messageRenderer.render(msg).foreach { newViewLayout =>
        dynamicLayout.foreach(content.remove(_))
        content.addComponentAsFirst(newViewLayout)
        dynamicLayout = Option(newViewLayout)
      }
    }
  }

  private def getMessage(msgId: Long, ui: UI): Option[DetailedDisplayMessage] = {
    if(SessionTracker.isLoggedIn(ui)) {
      displayMsgCache(SessionTracker.sessId(ui), msgId).genericHandle()
    } else None
  }

  private def render(msgId: Long, ui: UI): Option[DetailedDisplayMessage] = {
    implicit val uii = ui
    getMessage(msgId, ui)
      .map { msg =>
        val vt = ViewType.New
        val messageRenderer = renderer(msg, vt)
        messageRendererOpt = Some(messageRenderer)
        val content = getContent
        content.removeAll()
        update(msg, content, ui)
        msg
      }
  }

  override def setParameter(beforeEvent: BeforeEvent, mId: java.lang.Long): Unit = {
    this.mId = mId
  }
}
