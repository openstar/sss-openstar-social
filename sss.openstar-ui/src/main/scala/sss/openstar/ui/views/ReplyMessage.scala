package sss.openstar.ui.views


import com.vaadin.flow.component.orderedlayout.VerticalLayout
import sss.ancillary.Logging
import sss.openstar.ui.AppContext
import sss.openstar.ui.views.render.RenderMessageFactory.OnDone
import sss.openstar.ui.views.support.{AppContextSupport, AttachmentRenderSupport, RecipientsCostSupport, SessionIdSupport, WriteMessageSupport}


class ReplyMessage(
                   val onDone: OnDone)(implicit val appContext: AppContext)
  extends VerticalLayout
    with AppContextSupport
    with WriteMessageSupport
    with RecipientsCostSupport
    with AttachmentRenderSupport
    with Logging
    with SessionIdSupport {

}
