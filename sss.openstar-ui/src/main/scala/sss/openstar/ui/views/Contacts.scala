package sss.openstar.ui.views

import com.vaadin.flow.component.{Component, ComponentEventListener}
import com.vaadin.flow.component.grid.contextmenu.GridContextMenu.GridContextMenuItemClickEvent
import com.vaadin.flow.component.grid.{ColumnTextAlign, Grid}
import com.vaadin.flow.component.html.{Image, Label}
import com.vaadin.flow.component.orderedlayout.{FlexComponent, VerticalLayout}
import com.vaadin.flow.data.provider.ListDataProvider
import com.vaadin.flow.data.renderer.ComponentRenderer
import com.vaadin.flow.router.{BeforeEnterEvent, PageTitle, Route}
import sss.openstar.ui.rpc.ResultSeqContacts.RpcContact
import sss.openstar.ui.util.GridUtil
import sss.openstar.ui.views.support.{CurrentUISupport, NotificationSupport, PushSupport, RequireLoginSupportImpl, SessionIdSupport}
import sss.openstar.ui.{AppContext, BaseHelper, Helper, MainLayout}

import scala.jdk.CollectionConverters._
import scala.util.{Success, Try}
import NotificationSupport._

@Route(value = "contacts", layout = classOf[MainLayout])
@PageTitle("Contacts")
class Contacts() extends ViewFrame
  with SessionIdSupport
  with CurrentUISupport
  with PushSupport
  with RequireLoginSupportImpl
   {

  val contactGrid = new Grid[RpcContact]()
  contactGrid.setId("contactGrid")
  contactGrid.setSizeFull()

  val contactRenderer: ComponentRenderer[Component, RpcContact] = GridUtil.avatarRenderer(contact => contact)

  val contactCostRenderer = new ComponentRenderer[Component, RpcContact](contact => {
    new Label(contact.contactsCategoryAmount.toString)
  })

  val contactChargeRenderer = new ComponentRenderer[Component, RpcContact](contact => {
    new Label(contact.usersCategoryAmount.toString)
  })

  contactGrid.addColumn(contactRenderer)
    .setHeader("Contact")
    .setFlexGrow(1)
    .setAutoWidth(true)

  contactGrid.addColumn(contactCostRenderer)
    .setHeader("Cost To Send")
    .setFlexGrow(1)
    .setAutoWidth(true)

  contactGrid.addColumn(contactChargeRenderer)
    .setHeader("Charge To Receive")
    .setFlexGrow(1)
    .setAutoWidth(true)

  override def beforeEnterPostLogin(beforeEnterEvent: BeforeEnterEvent): Unit = {
    val dataProvider: ListDataProvider[RpcContact] = new ListDataProvider[RpcContact](
      BaseHelper.getContacts(sessId, false)
      .getOrElse(Seq.empty[RpcContact]).asJavaCollection
    )

    contactGrid.setDataProvider(dataProvider)
  }

  val contextMenu = contactGrid.addContextMenu()

  val deleteHandler: ComponentEventListener[GridContextMenuItemClickEvent[RpcContact]] = ev => {
    Try(ev.getItem.get) match {
      case Success(item: RpcContact) =>
        // TODO REMOVE CONTACT
        show("Not implemented yet")
      case _ =>
        // do nothing. They didn't select a contact.
    }
  }

  contextMenu.addItem("Delete", deleteHandler)

  val viewContent = new VerticalLayout(contactGrid)
  viewContent.setSizeFull()
  setViewContent(viewContent)
}

