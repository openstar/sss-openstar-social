package sss.openstar.ui.views


import java.io.InputStream
import java.io.InputStream

import com.vaadin.flow.component.{Component, Html, UI}
import com.vaadin.flow.component.html.{Div, Label, Span}
import com.vaadin.flow.component.icon.VaadinIcon
import com.vaadin.flow.component.orderedlayout.FlexComponent.JustifyContentMode
import com.vaadin.flow.component.orderedlayout.{FlexLayout, HorizontalLayout, VerticalLayout}
import org.jsoup.Jsoup
import org.jsoup.safety.Whitelist
import sss.ancillary.Guid
import sss.openstar.Bridger._
import sss.openstar.component.StyledText
import sss.openstar.ui.{AppContext, Helper}
import sss.openstar.ui.components.FlexBoxLayout
import sss.openstar.ui.rpc.ImplicitOps._
import sss.openstar.ui.rpc._
import sss.openstar.ui.util.CoinUtil.CoinComponent
import sss.openstar.ui.util.css.{BorderRadius, FlexDirection, TextAlign}
import sss.openstar.ui.util.{CoinUtil, FontSize, FormatUtil, ScrollTarget, UIUtils}
import sss.openstar.ui.views.ViewMessage.MessageStructure
import sss.openstar.ui.views.render.MessageRenderer
import sss.openstar.ui.views.render.RenderMessageFactory.ViewType.ViewType
import sss.openstar.ui.views.support._

object ViewMessage {

  case class MessageStructure(msgId: Long, readRecieptLayout: HorizontalLayout)

}

class ViewMessage(
                   viewType: ViewType)(implicit appContext: AppContext, sessId: SessionId)
  extends VerticalLayout
    with AttachmentRenderSupport
    with UserIdSupport
    with CurrentUISupport
    with MessageRenderer[DetailedEncryptedMessage]
    with BridgeLogging {
  setPadding(false)
  setSpacing(false)
  setSizeFull()


  private val scrollTarget = ScrollTarget("10px")
  private var messageStructures: Set[MessageStructure] = Set.empty

  val messagesWrapper = new FlexBoxLayout()
  messagesWrapper.addClassName("msg-wrapper")
  messagesWrapper.setSizeFull()

  add(messagesWrapper)

  private val reply = new ReplyMessage(
    () => ()
  )

  add(reply)

  private def buildAllMessages(msg: DetailedEncryptedMessage): Seq[FlexBoxLayout] = {
    val result = createMsgLayout(msg) +:
      msg.children.flatMap { childId =>
        appContext.bridge.fetchMessage(sessId.value, childId).genericHandle().collect {
          case child: DetailedEncryptedMessage =>
            buildAllMessages(child)
        }
      }.flatten

    result
  }

  private def fullRender(msg: DetailedEncryptedMessage): Option[Component] = {
    reply.setTos(msg.tos.toSet + msg.author - userId.value)
    //log.info(s"Reply ${msg.tos} msg.author ${msg.author} userId ${userId.value}")

    reply.setParentId(Some(msg.id))

    messagesWrapper.removeAll()
    val allMsgsRendered = buildAllMessages(msg)
    //log.info(s"TOT al num ${allMsgsRendered.size}")
    messagesWrapper.add(allMsgsRendered: _*)

    messagesWrapper.add(scrollTarget)
    ScrollTarget.scrollTo()

    Some(this)

  }

  private def syncReceipts(
                            messageStructure: MessageStructure,
                            newReceipts: Seq[ReadReceipt]): Unit = {
    messageStructure.readRecieptLayout.removeAll()
    messageStructure.readRecieptLayout.add(newReceipts.map(toReadReceiptControl): _*)
  }


  override def render(updatedMsg: DetailedEncryptedMessage): Option[Component] = {
    messageStructures.find(_.msgId == updatedMsg.id) match {
      case None => fullRender(updatedMsg)
      case Some(messageStructure) =>
        // the things that can update are 1. children and 2. readreciepts

        syncReceipts(messageStructure, updatedMsg.readReceipts)
        var needsToScroll: Boolean = false
        updatedMsg.children.foreach { childId =>
          appContext.bridge.fetchMessage(sessId.value, childId).genericHandle().foreach {
            case child: DetailedEncryptedMessage =>
              messageStructures.find(_.msgId == child.id) match {
                case Some(childMessageStructure) =>
                  syncReceipts(childMessageStructure, child.readReceipts)
                case None =>
                  //new child!
                  val c = createMsgLayout(child)
                  messagesWrapper.add(c)
                  needsToScroll = true

              }
            case x => log.warning(s"No idea how to render $x")
          }
        }
        if (needsToScroll) {
          messagesWrapper.remove(scrollTarget)
          messagesWrapper.add(scrollTarget)
          scrollTarget.scrollTo()
        }
        None
    }

  }


  private def createHeader(msg: DetailedEncryptedMessage): Component = {
    val result = new HorizontalLayout()

    result.setPadding(false)
    val author = new Label(msg.author.replaceAll("_", " "))
    author.addClassName("msg-author")

    val CoinComponent(div, _, _, _) = CoinUtil.createAmountComponent(msg.amountOpt.getOrElse(0L))

    val formattedDate = FormatUtil.toHumanReadableString(msg.receivedAt)
    val whenLbl = new Label(formattedDate)

    whenLbl.addClassName("msg-date")

    result.add(author, whenLbl, div)
    result
  }

  private def fileInputStreamProducer(guid: Guid, fileName: String): InputStream = {
    appContext.bridge.downloadAttachment(
      sessId,
      guid.value,
      fileName
    )
  }

  private def renderAttachments(guid: Guid, fileDetails: Seq[FileData]): VerticalLayout = {

    val attachLayout = new VerticalLayout()

    attachLayout.setPadding(false)
    attachLayout.setSpacing(false)
    if (fileDetails.nonEmpty) {

      attachLayout.addClassName("msg-attachment")
      attachLayout.add(fileDetails.map { file =>
        createAttachmentComponent(file.fileName, file.fileSize, file.fileType, () => fileInputStreamProducer(guid, file.fileName))
      }: _*)
    }
    attachLayout
  }

  private def createMsgLayout(detailedDisplayMessage: DetailedEncryptedMessage): FlexBoxLayout = {

    val header = createHeader(detailedDisplayMessage)

    //log.info(s"This child has ${detailedDisplayMessage.children.size} childrren")

    val attachments = renderAttachments(Guid(detailedDisplayMessage.guid), detailedDisplayMessage.files)

    val respectCRLF = detailedDisplayMessage.body.replaceAll("\\n", "<br>")
    val cleanText = Jsoup.clean(respectCRLF, Whitelist.basic().addEnforcedAttribute("a", "target", "_blank"))
    val messageBody = new StyledText(cleanText)

    messageBody.addClassName("msg-body")

    if (detailedDisplayMessage.author == userId.value) {
      messageBody.addClassName("msg-own")
    }

    val readRecsLayout = new HorizontalLayout()
    readRecsLayout.setJustifyContentMode(JustifyContentMode.END)

    readRecsLayout.add(detailedDisplayMessage.readReceipts.map(toReadReceiptControl): _*)

    val result = new FlexBoxLayout(header, messageBody, attachments, readRecsLayout)


    if (detailedDisplayMessage.author == userId.value) {
      result.addClassName("msg-own")
    }

    result.addClassName("msg")
    result.setFlexDirection(FlexLayout.FlexDirection.COLUMN)
    messageStructures += MessageStructure(detailedDisplayMessage.id, readRecsLayout)

    result
  }


  private def toReadReceiptControl(rr: ReadReceipt): Div = {
    {
      val l = new Label()
      UIUtils.setFontSize(FontSize.S, l)
      val whoPresentation = rr.who.replaceAll("_", " ")
      val icon = rr.whenDate match {
        case Some(d) =>
          l.setText(s"""${whoPresentation} • ${FormatUtil.toHumanReadableString(d)}""")
          new Label()
          val icon = VaadinIcon.CHECK_CIRCLE.create()
          icon.setColor("green")
          icon
        case None =>
          l.setText(s"${whoPresentation}")
          l.getStyle.set("color", "var(--lumo-tertiary-text-color)")
          val icon = VaadinIcon.CLOCK.create()
          icon.getStyle.set("color", "var(--lumo-tertiary-text-color)")
          icon
      }

      icon.setSize("14px")
      val wrapped = new Div(l, icon)
      wrapped.addClassName("msg-receiver")

      wrapped
    }
  }

}
