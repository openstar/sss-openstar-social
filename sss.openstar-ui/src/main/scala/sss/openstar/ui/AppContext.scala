package sss.openstar.ui

import sss.openstar.ui.rpc.{UserId}
import sss.openstar.ui.util.{DisplayMessageDataProvider, IdentitiesDataProvider, RegisteredProvidersDataProvider}
import sss.openstar.ui.views.render.{BrokenMessageRendererFactory, ContactInviteRenderer, Renderer, WriteMessageRendererFactory}

import scala.util.Try

trait AppContext extends BaseAppContext {

  implicit val appCtxt: AppContext = this

  lazy val renderer: Renderer = new Renderer(
    new WriteMessageRendererFactory(),
    new ContactInviteRenderer(),
    new BrokenMessageRendererFactory()
  )

  implicit lazy val displayMessageDataProvider = new DisplayMessageDataProvider()

  implicit lazy val identitiesDataProvider = new IdentitiesDataProvider()

  implicit lazy val registeredProvidersDataProvider = new RegisteredProvidersDataProvider()


}

object AppContext extends AppContext
