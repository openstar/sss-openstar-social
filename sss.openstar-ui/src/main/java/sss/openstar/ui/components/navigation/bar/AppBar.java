package sss.openstar.ui.components.navigation.bar;

import com.vaadin.flow.component.contextmenu.ContextMenu;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import sss.openstar.ui.BaseHelper;
import sss.openstar.ui.BaseMainLayout;
import sss.openstar.ui.Helper;
import sss.openstar.ui.components.FlexBoxLayout;
import sss.openstar.ui.components.navigation.tab.NaviTab;

public class AppBar extends BaseAppBar<AppBar> {

    public AppBar(BaseMainLayout<AppBar> parent, String title, NaviTab... tabs) {
        super(parent, title, tabs);

        initAvatar();
        initContainer();

    }

    public void initAvatar() {
        Image oldAvatar = avatar;
        avatar = BaseHelper.getUserAvatar();

        avatar.setClassName(CLASS_NAME + "__avatar");
        avatar.setAlt("User menu");

        if(oldAvatar != null) {
            container.replace(oldAvatar, avatar);
        }


        ContextMenu contextMenu = new ContextMenu(avatar);
        //contextMenu.set
        contextMenu.setOpenOnClick(true);

        contextMenu.addItem("Profile",
                e -> Helper.gotoProfile());

        contextMenu.addItem("Log Out",
                e -> {
                  if(getUI().isPresent()) {
                      BaseHelper.removeClientSideCredentialsAndLogout(getUI().get());
                  } else {
                      BaseHelper.logout();
                  }
                });
    }

    protected void initContainer() {
        container = new FlexBoxLayout(menuIcon, contextIcon, this.title,
                wallet, avatar);
        container.addClassName(CLASS_NAME + "__container");
        container.setAlignItems(FlexComponent.Alignment.CENTER);
        //container.setFlexGrow(1, contextIcon, wallet);

        getContent().add(container);
    }

}
