package sss.openstar.ui;

import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.polymertemplate.Id;
import com.vaadin.flow.component.polymertemplate.PolymerTemplate;
import com.vaadin.flow.templatemodel.TemplateModel;

/**
 * A Designer generated component for the tic-tac-toe template.
 *
 * Designer will add and remove fields with @Id mappings but
 * does not overwrite or otherwise change this file.
 */
@Tag("tic-tac-toe")
@JsModule("./tic-tac-toe.js")
public class TicTacToe extends PolymerTemplate<TicTacToe.TicTacToeModel> {

    @Id("r1c1")
    protected Button r1c1;
    @Id("r1c2")
    protected Button r1c2;
    @Id("r1c3")
    protected Button r1c3;
    @Id("r2c1")
    protected Button r2c1;
    @Id("r2c2")
    protected Button r2c2;
    @Id("r2c3")
    protected Button r2c3;
    @Id("r3c1")
    protected Button r3c1;
    @Id("r3c2")
    protected Button r3c2;
    @Id("r3c3")
    protected Button r3c3;
    @Id("vaadinVerticalLayout")
    protected VerticalLayout vaadinVerticalLayout;
    @Id("commitBtn")
    protected Button commitBtn;


    /**
     * Creates a new TicTacToe.
     */
    public TicTacToe() {
        // You can initialise any data required for the connected UI components here.


        vaadinVerticalLayout.setPadding(false);
        vaadinVerticalLayout.setSpacing(false);
    }

    /**
     * This model binds properties between TicTacToe and tic-tac-toe
     */
    public interface TicTacToeModel extends TemplateModel {
        // Add setters and getters for template properties here.
    }
}
