package sss.openstar.ui.views;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.html.Anchor;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import sss.openstar.ui.LoginSnippet;
import sss.openstar.ui.MainLayout;
import sss.openstar.ui.components.FlexBoxLayout;
import sss.openstar.ui.layout.size.Horizontal;
import sss.openstar.ui.layout.size.Right;
import sss.openstar.ui.layout.size.Vertical;
import sss.openstar.ui.util.UIUtils;
import sss.openstar.ui.util.css.FlexDirection;
import sss.openstar.ui.util.css.FlexWrap;

@Route(value = "home", layout = MainLayout.class)
@PageTitle("Welcome")
public class Home extends ViewFrame {

    public Home() {
        setId("home");
        setViewContent(createContent());
    }

    private Component createContent() {

        LoginSnippet login = new LoginSnippet();
        Anchor openstarSite = new Anchor("https://openstar.org", UIUtils.createButton("read more at openstar.org", VaadinIcon.EXTERNAL_LINK));
        openstarSite.setTarget("_window");

        FlexBoxLayout links = new FlexBoxLayout(openstarSite);
        links.setFlexWrap(FlexLayout.FlexWrap.WRAP);
        links.setSpacing(Right.S);

        FlexBoxLayout content = new FlexBoxLayout(login, links);
        content.setFlexDirection(FlexLayout.FlexDirection.COLUMN);
        content.setMargin(Horizontal.AUTO);
        content.setMaxWidth("840px");
        content.setPadding(Horizontal.RESPONSIVE_L, Vertical.L);
        return content;
    }

}
