package sss.openstar.ui;

import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.polymertemplate.Id;
import com.vaadin.flow.component.polymertemplate.PolymerTemplate;
import com.vaadin.flow.component.textfield.PasswordField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.templatemodel.TemplateModel;

/**
 * A Designer generated component for the login-snippet template.
 *
 * Designer will add and remove fields with @Id mappings but
 * does not overwrite or otherwise change this file.
 */
@Tag("login-snippet")
@JsModule("./login-snippet.js")
public class LoginSnippet extends PolymerTemplate<LoginSnippet.LoginSnippetModel> {

    @Id("username")
    public TextField username;
    @Id("unlockBtn")
    public Button unlockBtn;
    @Id("phrase")
    public PasswordField phrase;
    @Id("rephrase")
    public PasswordField rephrase;
    @Id("registerBtn")
    public Button registerBtn;
    @Id("loginLayout")
    private VerticalLayout loginLayout;
    @Id("shownotify")
    public Checkbox shownotify;

    /**
     * Creates a new LoginSnippet.
     */
    public LoginSnippet() {
        // You can initialise any data required for the connected UI components here.
        loginLayout.setJustifyContentMode(FlexComponent.JustifyContentMode.END);
        phrase.setRequired(true);
        phrase.setMinLength(8);
        rephrase.setEnabled(false);
        username.setRequired(true);
    }

    /**
     * This model binds properties between LoginSnippet and login-snippet
     */
    public interface LoginSnippetModel extends TemplateModel {
        // Add setters and getters for template properties here.

    }
}
