package sss.openstar.ui;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.server.PWA;
import sss.openstar.ui.components.navigation.bar.AppBar;
import sss.openstar.ui.components.navigation.drawer.NaviItem;
import sss.openstar.ui.components.navigation.drawer.NaviMenu;
import sss.openstar.ui.views.*;
import sss.openstar.ui.views.render.Renderer;

import java.util.Collection;

@CssImport(value = "./styles/components/rte.css", themeFor = "vaadin-rich-text-editor")
@PWA(name = "Openstar", shortName = "openstar", iconPath = "images/star_only_green_bk_smallest.png", backgroundColor = "#537aac", themeColor = "#a9bcd6")
public class MainLayout extends BaseMainLayout<AppBar> {

    public MainLayout() {
        super();
    }


    public static MainLayout get() {
        return (MainLayout) UI.getCurrent().getChildren()
                .filter(component -> component.getClass() == MainLayout.class)
                .findFirst().get();
    }

    protected void initNaviItems() {
        NaviMenu menu = naviDrawer.getMenu();
        menu.addNaviItem(VaadinIcon.INBOX, "In Box", InBox.class);

        NaviItem settings = menu.addNaviItem(VaadinIcon.SLIDERS, "Settings", null);
        menu.addNaviItem(settings, "Providers", Providers.class);
        menu.addNaviItem(settings, "Contacts", Contacts.class);
        menu.addNaviItem(settings, "Paywall", Paywall.class);
        menu.addNaviItem(settings, "Profile", Profile.class);
        menu.addNaviItem(settings, "Withdraw", WithdrawCurrency.class);
        menu.addNaviItem(settings, "Client Wallets", ClientWallet.class);
        menu.addNaviItem(settings, "API Token", CreateBearerToken.class);

        NaviItem createNew = menu.addNaviItem(VaadinIcon.EDIT, "New",
                null);

        Renderer renderer = AppContext$.MODULE$.renderer();
        Collection<Renderer.Catalog> iter = renderer.jCatalog();
        for(Renderer.Catalog c : iter){
            if(!BaseHelper.isBrokenRenderer(c.messagePayloadType())) {
                NaviItem item = new NaviItem(c.name(), Create.class, c.messagePayloadType().id());
                menu.addNaviItem(createNew, item);
            }
        }
    }

    @Override
    protected AppBar createAppBar() {
        return new AppBar(this,"");
    }


}

